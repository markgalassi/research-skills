First you need the general tools to make sphinx documents:

pip3 install --user sphinx sphinxcontrib-bibtex sphinx-rtd-theme sphinxcontrib-programoutput sphinx-multitoc-numbering

I use the cartouche extension

pip3 install --user cartouche sphinxcontrib-twitter

After installing that you can build the various formats of the book
with:

```
   make html
   make latexpdf
   make epub
   (cd ~/repo/research-skills && make html && rsync -avz --delete build/html/ ~/repo/pages/research-skills-html)
   (cd ~/repo/pages/ && git add research-skills-html && git add -u research-skills-html && git commit -a -m "updating web deployment" && git push)
```

http://3.bp.blogspot.com/-wblutbx_F0w/UnGTPz1futI/AAAAAAAAGLs/4VrJMrEmB7E/s1600/zoom.jpg
