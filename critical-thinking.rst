===================
 Critical Thinking
===================

Nuance and the Avoidance of Platitudes
======================================
Imagine you're looking at a painting. Maybe you're standing at a distance and
you have a beautiful perspective of the painting's composition. Maybe you're
standing as close as possible and you can see the detail and mastery in the
artist's brush strokes and color choices.

While both of these perspectives give you insight into the image and are
enjoyable in their own right, neither of them fully encapsulate the painting
as a whole. In order to truly understand the painting, you must look from a distance
and up close. So too with academic topics.

Part of critical thinking is seeing the nuance in any topic we learn about. Upon
initial observation, we only ever see a portion of any picture. In order to be
good researchers, and practice good critical thinking, it's crucial that we
understand that topics must be closely examined in their individual detail, and
also in the context of their field. The difference between academic topics and
paintings is that researching the real world never really gives you a nice frame
to bound your topic. Our world is so vast and interconnected that we can never
zoom out far enough, and at the same time understand with enough detail, that
our view of the picture can really be called "complete". Nuance and critical
thinking involves looking beyond the surface-level or simple explanations, and
knowing that we'll never know everything there is to know. It's about
understanding that things are never black or white, but instead are shades of
gray.

For example, this comic strip shows a few images
that change dramatically as the scope and perspective changes

http://3.bp.blogspot.com/-wblutbx_F0w/UnGTPz1futI/AAAAAAAAGLs/4VrJMrEmB7E/s1600/zoom.jpg

Notice how the images themselves are not altered, only the perspective of the viewer is.

How To See More of the Painting
-------------------------------

The observer at the art museum can walk around the painting in order to
gain perspective, but how can we as academic researchers do the same?

The easiest place to start is by recognizing limitations. Understanding
nuance involves recognizing that no singular source or study perfectly encapsulates
an entire topic or field.

This same reason is why it is so important to have multiple sources, a practice
often called triangulation. Triangulation is when multiple research methods
are used to validate findings. Just as the more places the art museum goer
stands in the room the better their understanding of the painting becomes, the
same goes for research findings.

Another wonderful way to address nuance is by discussion. Among peers or
professionals, discussing a topic will give you, literally and figuratively, as
many perspectives as possible. However, discussion requires other people, and
often people will avoid having to discuss nuances or approach disagreements by
using platitudes.

Platitudes
==========

A killer of nuance is the habit of using platitudes. Sports
journalists, managers in businesses and other administration tasks,
trade magazine writers, all fall in to this.

A platitude is an overused statement that might sound deep or meaningful,
but often lacks real significance. It's a cliche, it's vague, and it lacks
substance.

Platitudes are so detrimental to the understand of nuance because they hinder
meaningful discussions by catering to superficiality rather than real depth.
They frequently leave out or oversimplify complex issues into tweet-able sound bites.

To explain further, we here reproduce, with permission, a brief essay by Jerry Silfer who
is a public relations consultant and blogger. It makes this point nicely.

The Platitude Sickness: Taking Out the Trash of Corporate Speak
---------------------------------------------------------------

   :Author: **Jerry Silfwer**
   :Source: https://doctorspin.org/creative/storytelling-writing/platitude-sickness/

I sometimes hate what I do for a living.

A sizeable portion of what I write for clients will pass through
numerous of filters before getting published. And the end result is
nothing but a dwindling tirade of cringy corporate platitudes.

I’m not alone in feeling this way. We’re all exposed to corporate
speak. Whether you’re in marketing and communications or not, you’ll
see these platitudes everywhere. And for some reason, platitudes are
becoming the go-to format for many branded content strategies.



.. pull-quote::

   .. rubric:: According to Wikipedia:

   "A platitude is a trite, meaningless, or prosaic statement,
   generally directed at quelling social, emotional, or cognitive
   unease. The word derives from plat, the French word for “flat.”
   Platitudes are geared towards presenting a shallow, unifying wisdom
   over a difficult topic. However, they are too overused and general
   to be anything more than undirected statements with an ultimately
   little meaningful contribution towards a solution."

Corporate platitudes are such a waste of editorial
space. Unfortunately, the platitude sickness tends to do quite well in
social media.

A text loaded with obvious statements and no real knowledge can still
attract quite a lot of social engagement. People often hit that “Like”
button (or emoji-button or whatever) without even reading the actual
article it refers to.

"It’s important to have a strategy."

"Always put the customer first."

"Be proactive and think long-term."

"Publish epic content."

Their engagement reflects how they agree with the headline and how it
adds to their own personal worldview. It’s probably also a
psychological bandwagon-effect at play, a way of signal belonging to
important social circles.

So, how can you combat the platitude sickness in your corporate
communication?

.. pull-quote::

   Make it your personal mission to find platitudes and to destroy
   them. As this becomes a ritual, you’ll develop an “allergy” to
   corporate platitudes — and removing them will become second nature.

Welcome to the fight — I’m happy to have you onboard.


Another Rant Against Platitudes
-------------------------------

If you need further convincing, `The Speech Dudes
<https://speechdudes.wordpress.com/2015/10/20/politics-and-platitudes-the-no-shit-sherlock-test/>`_
have a strongly worded rant about the waste of life that platitudes
are. If nothing else, the amount of literature and articles on the
nuisances of platitudes should demonstrate to you not only how problematic
they are, but also just how prevelant. 

They first comment:

.. pull-quote::

   More often than not, a platitude simply states the obvious and so
   would be better off not having been uttered [1]

and footnote [1] is:

.. pull-quote::

   [1] With a platitude, not only is there a stating of the obvious
   but it’s also done in such as way as to have the appearance of
   being profound or wise. Facebook is full of such pre-digested
   pabulum that, sadly, spreads like linguistic herpes, passed on by
   well-meaning but ultimately uncritical people who think that
   quoting something that sounds smart also makes them sound smart. It
   doesn’t. Platitudes also seem to aspire to taking on a moral
   dimension, presumably to reinforce the semblance of profundity.

After personally insulting much of the population that posts on
Facebook, a social media platform popular among middle-aged and
elderly people, they go on to mention corporate *mission statement*
platitudes:

.. pull-quote::

   A mission statement such as "To combine aggressive strategic
   marketing with quality products and services at competitive prices
   to provide the best value insurance for consumers" is about as
   broad as you can get [... this really is the mission statement of a
   large insurance company]

They finally bring in a criminal organization as an example:

.. pull-quote::

   Here’s one you might have heard some years ago: Respect, integrity,
   communication, and excellence. This was from the company called
   Enron, which was one of the most notorious business scandals in
   American history and is considered by many historians and
   economists alike to be the unofficial blueprint for a case study on
   White Collar Crime. It’s also an example of a crime against
   vocabulary for creating such a miserably loose mission statement.

Clearly the Speech Dudes have an axe to grind here, but I find myself
in complete agreement that every example they give is awful.

As an exercise, you could visit `a rather sad site dedicated to
mission statements
<https://www.missionstatements.com/fortune_500_mission_statements.html>`_,
and go to the Fortune 500 Mission Statements page
[Fortune500MissionStatements]_ and pick out the few cases in which the
company's mission statement made you think "aha! this is original,
bold, and memorable". Consider, "what makes this better than the others?"
"What are they saying that is unique and meaningful?" "How did they say it
in a way that avoided platitudes?"
   

Mark's Personal Experience
--------------------------

I have worked in Los Alamos National Laboratory for my entire career.
Los Alamos started during the second world war: a group of scientists
who came together to design and build the first atomic bomb.

Later in its history, Los Alamos developed a more corporate atmosphere,
with layers of management and an administrative manual with tens of
thousands of pages.

As I was writing this section (May 2021) I looked up what our mission
statement was, and found:

.. pull-quote::

   Los Alamos National Laboratory's mission is to solve national
   security challenges through scientific excellence.

It surprised me for not being awful -- maybe just a bit too long.
The mission statement from earlier in my career (1994), formulated by
former Los Alamos director Sig Hecker, had been stronger, bolder, and
more challenging:

.. pull-quote::

   Reduce the Global Nuclear Danger.
   [Hecker1994]_

Simple, direct, and packing a punch.

The "Next Ten Words"
====================

Someone accustomed to critical thinking will often cringe at hearing
certain statements that are approximately 10 words long.

- "Quality is our first priority."
- "Our employees are our number one priority."
- "Delivering value is our number one priority." (Hmm, I'm beginning to
  wonder -- which is it?)
- "All's well that ends well."
- "It is what it is."
- "The ball is round." (a Dutch and German saying about soccer)
- "They are a valuable member of our team."
- "Workers should keep their money, not pay it in taxes."
- "They're going to have to start scoring some goals."

Some of these statements, like "it is what it is", or "the ball is round", 
are straight :index:`tautologies <single: tautology>` , meaning statements 
that are immediately and obviously true. Though they masquerade as statements
with substance, tautologies don't convey any actual information, at least not 
information worth repeating.

Others are what I call :index:`institutional tautologies <single:
institutional tautology>` , meaning statements that a manager, or someone
speaking for an institution, makes automatically whether they are true
or not. These statements are often meant to placate the listener, in order
to avoid having to execute any real change or actually solve any problems. 

Take, for example, the phrase "They are a valuable member of our team".
A manager is not allowed to tell the truth: "They are a burned-out
employee, and we keep shuffling them to different projects hoping that
a miracle will happen, but no miracles yet.  I'm really sorry that you
end up having to deal with them, and I hope you don't raise too much of
a stink about them."

Others are :index:`lazy <single: laziness>`: the phrase "workers
should keep their money, not pay it in taxes" has ten words.  It might
sound good, but you should be wondering what are the *next* ten words:
"does this worker drive a car on any public roads?" "Does this
worker use an airplane to go on vacation?" (to spend that hard-earned
money).

In your hunt for the "next ten words," you'd hopefully be led to the question
"if the tax structure were different, would this worker take home more or less
money in realistically?"  (By now we are being lax with the "ten" in "ten words".)

And the "next ten words" after that [#]_: "are there any examples of
people with higher and lower tax burden, and what is the outcome socailly
and individually?"

This last one is where we stop being lazy, we need to investigate if
a lower tax structure actually gives you better quality of life.

A first series of web searches will take us to a bunch of articles in
various online journals and magazines which give information about
state tax burdens (and the good ones will not only give state income
tax -- there are other state-level taxes!), affordability, and quality
of life.

With rapidly-obtained lists of state incomes (for example
[WikipediaListStatesByIncome]_) and state taxes (for example
[USATodayStatesByTax]_) we seem to find that the states with *more*
tax burden also have *higher* incomes, although there are exceptions.

We notice that those exceptions include Alaska and North Dakota.  Then
a thought, or more "next ten words", comes to the back of our head: 
"Do I remember something about Alaska giving every resident a check 
to share oil revenues?"  At this point we realize that there is too much 
nuance.  We suspend the "quick bit of research" with this provisional conclusion:

.. pull-quote::

   It appears that high state income tax might correlate with high
   income.  This means that higher taxes do not necessarily mean that
   a worker has less money to spend on what they want.  In fact, it
   might go the other way.  This would mean that the politician's ten
   word statement "workers should keep their money, not pay it in
   taxes" is a pandering phrase aimed at people who do not think
   critically about those statements.

We are almost ready to stop the quick dive: this temporary result is
an acceptable *interim* conclusion until we have time to do more
research. However, we don't stop quite yet. We first want to make sure
that we took mental note of what the limitations are of the research we
just did.  A few lingering questions are: (a) Is that first impression of
tax rates correlating with income quantitatively true? (b) Does
income capture all of quality of life? How about infrastructure? 
and (c) Are those numerical rankings good enough? Or would
more be captured by a more complete essay on quality of life in those
states?

The Investopedia article "9 States With No Income Tax"
[InvestopediaTaxBurden]_ seems to cover this ground. They first give
quick figures of the state tax burden rank.  Putting that article
together with the others might give some interesting insights.

Since this has become a real mini research project we will leave
further analysis to Chapter :ref:`research-examples:research
examples`, Section :ref:`research-examples:Taxation, disposable
income, quality of life`.  In that chapter we will go in to a bit more
detail, including poking at the credibility and political bias of the
news outlets that published the articles we mention.

I will conclude by pointing out that knowing how to produce this kind
of "quick research" to a superficial statement is maybe the most
important of our goals. Ff you learn to get several articles, vet
their sources, and reach a deeper conclusion, then you are on the way
to being free of misinformation, and free from trusting dangerously
shallow reporting.

Groupthink and red team exercises
=================================

Groupthink is an insiduous psychological phenomenon in which a group
of people start making irrational or dysfunctional decisions.  People
in a group, typically driven by a need to feel close to others and a 
prioritization of harmony above all else, will conform their thinking 
to what appears to be the group's opinion.

The term is inspired by the dystopian political fiction from the
mid-20th-century: Orwell's novel 1984. The novel described deliberate
brainwashing of the population by the authoritarian leadership.
While the pyschological phenonmena of groupthink is not always deliberate, 
or forcefully imposed, it still results in a similar conforming of views.

Research psychologist Irving Janis applied his theory of groupthink to
study a whole collection of policy fiascos, including Nazi Germany's
decision to invade the Soviet Union, the Bay of Pigs invasion, and the
escalation of the war in Vietnam.

Historians and social psychologists have written extensively about
this, and debate the role of groupthink in these events.  A broad and
deep reading of this subject can be though-provoking and rich in
insight.

More recently, groupthink undertones can be found in online communities,
specifically in memes. The term "meme" was first coined by the british evolutionary 
biologist Richard Dawkins in his 1976 book "The Selfish Gene". Though published
long before the emergeance of internet culture, Dawkins used the term to describe 
an idea, behavior, or cultural practice that spreads from person to person through 
immitation. Coming from the Greek word "mimema," "something imitated," the term has 
been adopted from it's orginal use to refer to rapidly spread or replicated images or jokes.
Often, this quick change of information on the massive scale that is the internet can 
result in the reinforment of exisitng biases, echo chambers, and the suppression of 
dissenting opinions. Is a meme of a cute kitty going to make you part of groupthink? No,
of course not. However, darker recesses of the internet may use memes, information that
screams "Copy me! Immitate me!" to create groupthink driven environments. When memes venture
into the dark and begin to blur fact and satire, danger can arise. 

Critical thinkers need to be aware of the possibility of groupthink
when they see anything that is the result of a group of people
pondering an issue.  They should look beyond the surface and make sure
that the group was chartered with explicit awareness of this danger.

To avoid the dangers of groupthink the United States Department of
Defense created the idea of ":index:`red team` exercises".  A "red
team" plays the role of an opposing side, fully immersed in the
opposition's goals and not influenced by one's own side.  Red teaming
is now used in many areas of military and cybersecurity activity, as
well as being used in some scientific projects, where a separate team
is created to look for problems with a scientific software
architecture.

Red teaming is important, but in Section :ref:`goals:The "Next Ten
Words"` we pointed out that we should always ask what comes next. A breezy
statement of "we have red-teamed this ..." does not mean that we are
safe. Management chains will often send the results of a red team to
an upper layer of management without emphasizing the problems that
were discovered.

Two vivid examples of red team exercises that were ignored are a 1932
simulation of an attack on Pearl Harbor, and some of the results from
a review of Boston's Logan Airport security conducted before the 9/11
terrorist attacks, where two of the hijacked 9/11 airplanes departed from.

The ways in which red teams get ignored is shown in one
whistleblower's testimony to the US Congress during the 9/11 hearings:

.. pull-quote::

   The bottom line of FAA's response to its Red Team findings is that
   the Red Team was gradually working its way out of a job. The more
   serious the problems in aviation security we identified, the more
   FAA tied our hands behind our backs and restricted our
   activities. All we were doing in their eyes was identifying and
   "causing" problems that they preferred not to know
   about. [DzakovicNineElevenTestimony]_

Red Team Exercises can provide a solution to the dreadful groupthink, but,
like anything, it can only be effective if executed properly. 


.. rubric:: Footnotes

.. [#] The rhythmic emphasis of the "next ten words" comes from
   fictitious President Bartlett, in the television series The West
   Wing.
