==============
 Bibliography
==============

..
   motivation and plan

.. [Pais1982] Abraham Pais, 1982, `"Subtle is the Lord...": The
   Science and Life of Albert Einstein`

..
   goals

.. [WikipediaOpAmp]
   https://en.wikipedia.org/wiki/Operational_amplifier


.. [Hecker1994] HECKER, SIEGFRIED S. “Retargeting the Weapons
   Laboratories.” Issues in Science and Technology, vol. 10, no. 3,
   1994, pp. 44–51. JSTOR, www.jstor.org/stable/43311403. Accessed 25
   May 2021.

.. [Fortune500MissionStatements]
   https://www.missionstatements.com/fortune_500_mission_statements.html

.. [DzakovicNineElevenTestimony]
   https://govinfo.library.unt.edu/911/hearings/hearing2/witness_dzakovic.htm

.. [InvestopediaTaxBurden]
   https://www.investopedia.com/financial-edge/0210/7-states-with-no-income-tax.aspx

.. [WikipediaListStatesByIncome]
   https://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_income

.. [USATodayStatesByTax]
   https://www.usatoday.com/story/money/2020/04/19/taxes-2020-states-with-the-highest-and-lowest-taxes/111555224/

.. [NewYorkerKahnemanTversky]
   https://www.newyorker.com/books/page-turner/the-two-friends-who-changed-how-we-think-about-how-we-think

.. [JuliaGalefBayesianThinkingVideo]
   https://www.youtube.com/watch?v=BrK7X_XlGB8

.. [JuliaGalefTEDxPSU]
   https://www.ted.com/talks/julia_galef_why_you_think_you_re_right_even_if_you_re_wrong/up-next

.. [galef2021scout]
   ::

      @book{galef2021scout,
        title={The Scout Mindset: Why Some People See Things Clearly and Others Don't},
        author={Galef, Julia},
        year={2021},
        publisher={Penguin}
      }

.. [GalefMatthewsInterviewVox]

   https://www.vox.com/future-perfect/22410374/julia-galef-book-scout-mindset-interview-think

.. [banaji2016blindspot]
   ::

      @book{banaji2016blindspot,
        title={Blindspot: Hidden Biases of Good People},
        author={Banaji, M.R. and Greenwald, A.G.},
        isbn={9780345528438},
        lccn={2012015905},
        url={https://books.google.com/books?id=z6WvDAAAQBAJ},
        year={2016},
        publisher={Bantam Books}
      }

.. [BanajiProjectImplicit]

   https://implicit.harvard.edu/implicit/takeatest.html

.. [BarbaFrustrationDiversityEffortsInSTEMVideo]

   https://www.youtube.com/watch?v=THf8_A-RK38

.. [BarbaFrustrationDiversityEffortsInSTEMSlides]

   https://lorenabarba.com/figshare/one-step-forward-two-steps-back-the-frustration-of-diversity-efforts-in-stem/

.. [Hartman2009IneffectiveTreatmentsSeemHelpful]
   https://chiromt.biomedcentral.com/articles/10.1186/1746-1340-17-10

..
   research examples

.. [RedditKingHenryEclipse]
   https://www.reddit.com/r/badhistory/comments/6v924g/bad_nasa_eclipse_history_or_how_henry_i_of/
