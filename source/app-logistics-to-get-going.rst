.. _app-logistical-details:

==============================
 Appendix: logistical details
==============================

.. caution::

   This section is a work in progress.

There are some tools a student should always have handy to use in the
ordinary course of their studies.  These include a plotting program,
the Python interpreter, and the ``git`` version control program.

RAWGraphs is a free and open source webapp that can be found at
https://app.rawgraphs.io.

There are others that are useful for specific courses, tasks, or
jobs.

Linux
=====

Arch Linux
^^^^^^^^^^
``$ sudo pacman -S git python``

Debian
^^^^^^
``$ sudo apt-get update``
``$ sudo apt-get install git python3``

Fedora
^^^^^^
``$ sudo dnf install git python3``

FIXME: unfinished, but the outline is to show how to install each of
these programs on Linux, Chromebook, Windows, and MacOS.
