===========================================
 Appendix: List of articles for discussion
===========================================

.. caution::

   This appendix is a work in progress.


Media bias diagram
==================

https://upload.wikimedia.org/wikipedia/commons/0/07/Media-Bias-Chart_4.0.1_WikiMedia_Commons_Copy.jpg

https://mediabiasfactcheck.com/

https://mediabiasfactcheck.com/the-balance/

League of women voterse discussion of media bias charts:

https://my.lwv.org/california/torrance-area/article/how-reliable-your-news-source-understanding-media-bias-2022


Pushing at an open door
=======================

Report from the CDC, a trusted organization:

https://www.whitehouse.gov/briefing-room/press-briefings/2021/04/27/press-briefing-by-white-house-covid-19-response-team-and-public-health-officials-32/

Critical look at the CDC report:

https://www.nytimes.com/2021/05/11/briefing/outdoor-covid-transmission-cdc-number.html

https://statmodeling.stat.columbia.edu/2021/05/11/if-a-value-is-less-than-10-you-can-bet-its-not-0-1-usually/


Selection effects
=================

https://www.cnbc.com/2021/05/13/seychelles-most-vaccinated-nation-on-earth-but-covid-19-has-surged.html


Taxation policy
===============

https://www.usatoday.com/story/money/2020/04/19/taxes-2020-states-with-the-highest-and-lowest-taxes/111555224/

https://www.epi.org/publication/ib338-fiscal-cliff-obstacle-course/

https://taxfoundation.org/which-states-have-most-progressive-income-taxes-0/


https://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_income

https://www.investopedia.com/financial-edge/0210/7-states-with-no-income-tax.aspx



Rennaissance Capital
====================

http://lib.21h.io/library/P4IH7WWY/download/UDUBAIW5/Zuckerman%20-%202019%20-%20The%20man%20who%20solved%20the%20market.pdf
