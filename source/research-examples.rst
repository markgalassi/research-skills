===================
 Research examples
===================

.. caution::

   This section is a work in progress.


Things to have handy before you do research
===========================================

https://upload.wikimedia.org/wikipedia/commons/0/07/Media-Bias-Chart_4.0.1_WikiMedia_Commons_Copy.jpg



Worked examples
===============

Taxation, disposable income, quality of life
--------------------------------------------

See the discussion in :ref:`research-step-by-step:How do we get to our
research question?` for this interesting research topic.  At this time
we give you some reference links below.

Here is an example of how you could carry out some research on that
statement to get a more nuanced understanding of taxation and an
individual's quality of life and purchasing power.

[FIXME: unwritten below here]

https://www.cbo.gov/

https://www.cbo.gov/publication/57170

https://www.jct.gov/publications/


tax revenue:

https://www.thebalance.com/current-u-s-federal-government-tax-revenue-3305762

https://www.cbo.gov/about/products/budget-economic-data#2

https://www.brookings.edu/wp-content/uploads/2016/06/09_effects_income_tax_changes_economic_growth_gale_samwick.pdf

detailed study:

https://eml.berkeley.edu/~dromer/papers/RomerandRomerAERJune2010.pdf

popular article but quite complete:

https://www.investopedia.com/articles/07/tax_cuts.asp

quality of life and infrastructure:

https://www.usatoday.com/story/money/2020/04/19/taxes-2020-states-with-the-highest-and-lowest-taxes/111555224/

https://www.epi.org/publication/ib338-fiscal-cliff-obstacle-course/

https://taxfoundation.org/which-states-have-most-progressive-income-taxes-0/


https://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_income

https://www.investopedia.com/financial-edge/0210/7-states-with-no-income-tax.aspx

The King Henry I Eclipse
------------------------

There is a reddit thread called "Bad NASA Eclipse History, or How
Henry I of England did not die twice" at [RedditKingHenryEclipse]_

This is a nice worked example of a puzzle that shows sloppy reporting
that has made its way into numerous articles as if it were fact.

The fivethirtyeight data sets and examples
------------------------------------------

The fivethirtyeight team of data journalists make their data sets
available, together with the articles they write about what they glean
from the data.

The data for their worked examples can be found at:

https://data.fivethirtyeight.com/


President Biden's comment on the Marijuana suspension of Richardson
-------------------------------------------------------------------

Backdrop: shortly before the Tokyo "2020" olympics (which are to take
place in the summer of 2021) US sprinter Sha'Carri Richardson, one of
the fastest 100 meter athletes in the world, tested positive for THC,
the active ingredent in marijuana.  Repercussions of this test led to
a 30 day suspension from racing, which would keep her out of most of
the olympic races, including the 100 meters.

President Biden was asked what he thought of Richardson's suspension.
Part of his reply (where he said "the rules are the rules") was posted
to wtitter by someone, and that reply was then "re-tweeted" by highly
regarded statistician Nate Silver, who added a negative comment.

The comment by Nate Silver is at
https://twitter.com/NateSilver538/status/1411418022237990912 and I
show it here:

::

   Nate Silver
   @NateSilver538
   Kinda surprised by this from Biden, I'd imagine her suspension is quite unpopular.

   Jennifer Jacobs
   @JenniferJJacobs
   Jul 3, 2021
   "The rules are the rules," Biden says in Michigan when asked about
   Sha' Carri Richardson's one-month suspension for marijuana
   use. @itskerrii will miss the 100m race during the Olympic games in
   Tokyo.

I list this in this "worked examples" section because the thread on
twitter has a discussion that touches upon many of the questions we
might think to ask, and has reflections and conclusions.

Examining the thread brings out several important discussion points
and a collection of facts and opinions about the situation,
Richardson, Biden, and Silver.

Some points to think about in this exchange are:

* Some comment on how she knew the framework she works in.
* Some people comment on the appropriateness of THC bans.
* Some comment on how the IOC (International Olympic Committee) does
  not follow US law, but rather its own regulations.  In addition they
  point out that THC can be a "masking agent", so it might stay banned
  even if the US lifts its ban.

Note that all these points need to be looked at carefully, but maybe
the most important point is made further down in the thread:

* Someone points out that Biden's comment was much more detailed and
  nuanced than "the rules are the rules".

  The full statement was:

     *Everybody knows of the rules going in. Whether they should remain the
     rules is a different issue, but the rules are the rules.*

     *I was really proud of the way she responded.*

One can also find video clips of this, which confirm the more complete
statement by president Biden.

Below I give some of the raw text from the twitter thread.  My goal is
to get you to think of which comments provide insight.

But it is also to make you think critically of where those comments
end, and what they might be missing, and how they might be grossly
incorrect.

Remember how in Chapter :ref:`reading-effectively:Where Is This Source
Coming From?` we lambasted lazy
journalists for not asking the next question.  We would be as lazy as
them if we did not ask the next question about this statement:

   *Why would it move the ioc? [IOC is the International Olympic
   Committee] Many legal things are banned by the ioc because they are
   considered masking agents. The ioc doesn't care about the federal
   law in the us.*

::

   Jeff C Red circleBlack circleRed circle
   @JMC9787
   ·
   Jul 3
   Replying to 
   @NateSilver538
   I personally don’t understand all the outrage. Should marijuana be
   allowed at this point? Sure. But she knew the rules and knew it
   wasn’t allowed and did it anyway. Zero personal accountability. She
   made her own bed.


   Sean Jones
   @sprov4
   ·
   Jul 3
   Why punish people for not following a pointless rule?

   Jedi, Interrupted Rainbow flagYellow heart
   @JediCounselor
   ·
   22h
   Replying to 
   @NateSilver538
   FULL QUOTE:

   “Everybody knows of the rules going in. Whether they should remain
   the rules is a different issue, but the rules are the rules."

   “I was really proud of the way she responded,” he adds.

   Did you pop off without bothering to get the context again, Nate?


   Dr. Berenger
   @Frenchesque
   ·
   20h
   Thank you for this.  Silver’s tweet makes it sound like Biden
   suspended her himself.
   It’s irresponsible.

   Mike Malloy
   @MikeTuesday2
   ·
   Jul 3
   Replying to 
   @NateSilver538
   That forbidden “rule” would seamlessly go away with federal drug
   reform of THC. Gee Joe, perhaps you should do something about that
   already.


   Jaime Robledo
   @RobledoReturns
   ·
   23h
   Literally the next sentence is him questioning whether those laws
   should stand.  Why a polling aggregator is offering opinions on
   Olympics drug policy anyway is beyond me.


   Ron Wechsler
   @RonWechsler
   ·
   Jul 3
   Replying to 
   @NateSilver538
   Suspect this isn’t the full quote.


   Jedi, Interrupted Rainbow flagYellow heart
   @JediCounselor
   ·
   22h
   Replying to 
   @NateSilver538
   FULL QUOTE:

   “Everybody knows of the rules going in. Whether they should remain
   the rules is a different issue, but the rules are the rules."

   “I was really proud of the way she responded,” he adds.

   Did you pop off without bothering to get the context again, Nate?
   Dr. Berenger
   @Frenchesque
   ·
   20h
   Thank you for this.  Silver’s tweet makes it sound like Biden suspended her himself.  
   It’s irresponsible.
   Dave Total Landscaping
   @Save_the_Daves
   ·
   23h
   Replying to 
   @NateSilver538
   He’s not wrong. You can think the rule is dumb while still
   acknowledging she was wrong for knowingly violating it. She
   shouldn’t have to stay home for smoking marijuana. She absolutely
   should stay home for violating a drug policy she knew full well
   would get her DQ’d.
   Matthew
   @acadianrunner
   ·
   22h
   Ya. Keep her home based on a rule that is archaic and based on racism. Great look America
   Dr. Berenger
   @Frenchesque
   ·
   20h
   Marijuana usage isn’t racially determined and the Olympics isn’t American. 
   I do hope they change the rules.
   Dave Total Landscaping
   @Save_the_Daves
   ·
   19h
   Agree with all of this
   ConanTheCnidarian
   @CCnidarian
   ·
   Jul 3
   Replying to 
   @NateSilver538
   I mean, what is he supposed to say? It’s unfortunate that the rules
   are the way they are, but he has a lot more to worry about than
   whether marijuana disqualifies you from international track
   meets... is Biden supposed to lead the movement to change the track
   marijuana rules?
   Jamie McCurdy
   @ackbar7
   ·
   Jul 3
   He should lead a movement to change those rules and laws in our country
   josh valentine
   @jjv124
   ·
   Jul 3
   He said the rules are the rules but whether they should remain that way is a different thing
   Jamie McCurdy
   @ackbar7
   ·
   Jul 3
   Sure, but if he doesn't like them why isn't he doing more to change
   the laws here in the US? The question was what was he supposed to
   do, and the US legalizing marijuana would probably do a lot to move
   the IOC in that direction as well.
   josh valentine
   @jjv124
   ·
   Jul 3
   Why would it move the ioc? Many legal things are banned by the ioc
   because they are considered masking agents. The ioc doesn't care
   about the federal law in the us


You can use display-thread flag to display replies.


Partially worked examples
=========================

Poverty, scarcity mindset, efefective altruism, moral ambition
--------------------------------------------------------------

The hook
~~~~~~~~

It starts this morning (2024-07-10) when I find an article in one of
the morning news sources I receive (this one is `TL;DR
<https://tldr.tech/>`_).  This one is `titled Effective altruism is
stumbling. Can "moral ambition" replace it?
<https://bigthink.com/high-culture/effective-altruism-moral-ambition/>`_

I start reading the article because I am curious about what the next
forumulation of Effective Altruism will be, after its popularity
problems -- notorious cryptocurrency criminal Sam Bankman-Fried had
been one of its most famous promoters.  (This in itself is an
interesting matter: Peter Singer has not been disgraced, and it will
be interesting to see how the Effective Altruism will fare...)

The article starts with a discussion of abolitionism in the late
1700s, and introduces Dutch writer Rutger Bergman and his 2017 TED
talk `Poverty isn't a lack of character; it's a lack of cash
<https://www.ted.com/talks/rutger_bregman_poverty_isn_t_a_lack_of_character_it_s_a_lack_of_cash>`_.
I have, and am reading, Bregman's book "Utopia for Realists".

Experiments in the effects of poverty
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I'm beginning to think that I could carry this out as an example for
this chapter, so I start taking notes on the links I find and the
passages that interest me.

I now move to taking my cues from the TED talk, though I am as usual a
bit annoyed by the slowness of a video compared to written material.
Still, TED talks are brief (this one is some 15min long).

The first item he mentionst that I want to look up is Eldar Shafir,
the Princeton psychologist who promotes the theory of "psychology of
scarcity" - people with scarce resources have to make continuous
decisions on trade-offs between things.  The collection of conditions
that accompany poverty results in a 14-point IQ drop.  Note that
Shafir uses the example of a computer that is overloaded because it is
doing too much computation, which reminds me of Tamim Ansary's
discussion of technological metaphor in "The Invention of Yesterday".

Shafir did experiments on Princeton students, but more interesting is
the sugar cane harvester experiment, where the peculiar nature of the
business means that there is a period (after the harvest) in which
workers feel comfortable with their income.  In the time leading
up to the harvest, instead, they feel economicallystrained.  IQ tests
done before and after the harvest show a 14 ponit difference.  Shfir
has `has his own TED talk
<https://www.youtube.com/watch?v=gV1ESN8NGh8>`_ in which he discusses
these experiments.

The other item mentioned by Bregman is the famous "Mincome" social
experiment in the small Canadian town of Dauphin.  I look it up on
Wikipedia and find that:

The provice of Manitoba and the Canadian federal government provided a
minimum income guarantee to all residents of the town.  The experiment
was dropped after 4 years when the ruling parties lost both provincial
and federal elections.

The experiment was analyzed later by economist Evelyn Forget who found
that several metrics of well-being in Dauphin improved significantly
during the years of the experiment.  Two things are noted by the
Wikipedia article: (a) she did not use much direct data from the
experiment - she simply used the fact that a significant fraction of
the population of the town hard participated; (b) she stated a
correlation, did not identify specific causal factors, but did compare
to control groups in Winnipeg and the rest of Manitoba.

I now go to one of her papers directly, a commentary called `The Town
with No Poverty: The Health Effects of a Canadian Guaranteed Annual
Income Field Experiment
<https://web.archive.org/web/20240109192405/https://www.utpjournals.press/doi/pdf/10.3138/cpp.37.3.283>`_
In this paper, after discussing the backdrop of "MINCOME" experiments
and proposals, she goes on to show metrics of students school
performance, hospitalization for "accident and injury", birth rates of
children to young mothers.  She states in her conclusion that "[...]
the measured impact was larger than one might have expected when only
about a third of families qualified for support at any one time".

Moving on: "effective altruism" versus "moral ambition"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

At this point I pivot away from the poverty experiments and back to
the original point of the article, which was a review of Bregman's new
book on "moral ambition", but first I list (to read some time later)
some links I collected while looking up "scarcity mindset".  I have
not yet evaluated these links and they might be duds:

https://www.thecrimson.com/column/a-time-for-new-ideas/article/2020/5/1/gilbert-breaking-down-scarcity-mindset/

https://due.com/a-scarcity-mindset-feasts-on-generational-poverty/

https://www.apa.org/monitor/2014/02/scarcity

I now continue with the `original article
<https://bigthink.com/high-culture/effective-altruism-moral-ambition/>`_
and things start homing in on the main topic: effective altruism
versus Bregman's new proposal for "moral ambition".

The *effective altruism* (EA) movement founded by Peter Singer gets
criticized in this article: Bregman points out that the good aspects
of EA (promoting non-profit charity work) are accompanied by a coating
of guilt.  Bregman uses the expression "moral blackmailing".

Bregman's proposal in the book *Moral Ambition* is a call for people
to be activists, trying to form "small, committed, idealistic groups
of people".  He criticizes a frequent leftist point of view that
"everything should start with the system, with the structural causes
of poverty and inequality."  He feels that it is a negative pointing
to others, rather than a call for someone to take action.  He proposes
a more positive approach.

A few more points that come from the article:

* Bregman claims that "awareness is so overrated", and activism is
  expressed in negative instead of positive terms.
* There is a reference to David Graeber's concept of "Bullshit Jobs"
  which is intriguing: it is in the context of how Bregman believes
  that people have an inherent desire to do idealistic work.
* Bregman has founded "The School for Moral Ambition" which offers
  fellowships for people to work in promoting morally ambitious
  entrepreneurship.

Concluding thoughts
~~~~~~~~~~~~~~~~~~~

Here are some thoughts I have after just a couple of hours of
researching this topic.

I definitely want to read the book; my public library does not yet have it.

I should ask my Dutch friends how Bregman is seen there.

I like the framing of a positive and hard-working movement, rather
than a complaint-based approach.

The creation of the School for Moral Ambition is a way of standing
behind the idea in the book.  I'm impressed.

Have there been other attempts at channeling these kinds of good
intentions?

I think this is an important piece of counterpoint within the
"utilitarianism" movement.

Apart from reading the book itself, I also need to find people who
have criticized Bregman's new moral ambition movement.

Unworked examples
=================


Instant messaging and privacy
-----------------------------

Article that asks real questions and goes in to detail:

https://www.bloomberg.com/news/articles/2021-05-28/signal-app-is-surging-in-popularity-and-hitting-growing-pains


Texas mask mandate mystery
--------------------------

(this is an old one from 2021; in 2024 and beyond it's less likely to
be an interesting one)

https://www.theatlantic.com/ideas/archive/2021/05/texas-mask-mandate-no-effect/618942/


Seychelles Islands vaccine mystery
----------------------------------

In May 2021 the Seychelles had succeeded in vaccinating more of their
population for the COVID-19 virus than any other country on earth.
And yet at the same time they had a great increase in COVID-19
cases.

Read the news articles, and possibly scholarly articles, and apply
your understanding of selection effects, described in Chapter
:ref:`cognitive-factors:Cognitive Errors and Selection Effects`,
Section :ref:`cognitive-factors:Selection Effects and the "Hidden
Prior"`.  Prepare a few paragraphs on what might be going on.

This exercise might not give a definitive result in the spring of
2021, since it is a new phenomenon, but you could turn that into a
strength for this exercise: get used to having a partial and
provisional understanding of a topic, that can adapt gracefully as new
information comes in.


Julia Galef's hidden prior experiment
--------------------------------------

Chapter :ref:`cognitive-factors:Cognitive Errors and Selection
Effects`, Section :ref:`cognitive-factors:Selection Effects and the
"Hidden Prior"`

Take the "mathematics grad student or business school student" thought
experiment that Julia Galef gives at
[JuliaGalefBayesianThinkingVideo]_.  Research the actual ratio of math
graduate students and business school students at a few US
universities.

Be careful to choose representative samples of academic programs --
some universities might be skewed for or against pure mathematics or
business.

And maybe you can even research the actual ratio of shy
mathematics students to shy business students.

With this real data in hand, see if you can confirm or disprove
Galfe's thought experiment.
