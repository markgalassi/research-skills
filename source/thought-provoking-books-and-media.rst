===================================
 Thought-provoking books and media
===================================

.. caution::

   This section is mostly finished.

Here I propose a list of some books which might inspire thinking
beyond their specific subject matter.

This "thinking beyond..." can be due to (a) an approach to the subject
matter that brings several disparate fields together, or (b) a way of
finding angles of analysis of a topic that expand our way of thinking
about any subject.

Some words about the choice of books:

The list is clearly arbitrary: I have put down books I am familiar
with and which I found thought-provoking.  The most obvious "selection
effect" is my personal preference in reading.

I don't agree with everything that is said in all of them, and
sometimes I don't even agree with the book's overall thesis, or the
way the author has chosen to argue it, or even the tone.  Still, they
can be though-provoking, and reading them can be a part of your own
dialectic in how you think about a subject.

For example, "The Dawn of Everything" seems to use what I sometimes
characterize as a "I have a big chip on my shoulder" approach: the
authors are proposing a different view of prehistoric civilizations
from a mainstream narrative, and they feel the need to frequently
point out how wrong the proponents of other narratives are.  When I
read these passages I found them awkward: they impeede the flow, and
leave me less convinced than I might be otherwise.  It is also
"clannish": it limits the efficacy of that discussion to those who
have read the books they are criticizing.  Still, I think it is
important to be aware of their point of view, and I would recommend
reading the book if you want to do a full tomography of early
civilization.

Sometimes the book shows how the author has put careful thought into
how to present the material.  In the introduction to "The Story of
Art", for example, Gombrich writes:

   "[...] I have tried, in writing this book, to follow a number of
   more specific self-imposed rules, all of which have made my own
   life as its author more difficult, but may make that of the reader
   a little easier."

Other times I have found that a book is "intellectually not lazy".
For example, although one might argue with some specifics, Jared
Diamond's book "Collapse" is a *tour-de-force* that does not skimp on
explaining methodology and sources.

.. sidebar:: How to *read* these resources

   There are many articles and books on how to read books.  In this
   vast space of opinions there is really no need for mine, but I will
   put my oar in just a little bit, and express some opinions while I
   am at it.

   An article I have found to be **good** (and also brief!) is Paul
   Edwards's "How to Read a Book" (I read it at version 5.0).  You can
   can `find it here
   <https://pne.people.si.umich.edu/PDF/howtoread.pdf>`_ [#]_ It
   points out the fundamental difference between reading fiction
   (where you have to read in order, since it is building up suspense)
   and nonfiction where you can and should dart around.

   Edwards puts it really well (and concisely!) with gems like:

      So unless you're stuck in prison with nothing else to do, NEVER
      read a non-fiction book or article from beginning to end.

      Instead, when you’re reading for information, you should ALWAYS
      jump ahead, skip around, and use every available strategy to
      discover, then to understand, and finally to remember what the
      writer has to say. This is how you’ll get the most out of a book
      in the smallest amount of time...

   Almost all the books and articles I'm proposing here are
   non-fiction, so please attack them with the optimization goal that
   Edwards proposes.

   I also recommend skipping the awful book by Adler and Van Doren
   "How to Read a Book".  This is one of several awful books that has
   acquired the status of being a "classic" and gets recommended out
   of group-think.

   .. rubric:: Footnotes

   .. [#] In case the published copy moves away I also offer a copy
          for download here:
          :download:`PaulEdwards-HowToReadABook_v5.0.pdf`  I can do
          this because the author did the awesome thing of offering it
          under a creative commons BY-SA license.

.. contents:: Sections inside this page
   :depth: 1
   :local:
   :backlinks: none


Books: non-fiction
==================

History
-------

Tamin Ansary
   The Invention of Yesterday: A 50,000-Year History of Human Culture,
   Conflict, and Connection

Barbara Tuchman
   A Distant Mirror: The Calamitous 14\ :sup:`th` Century

   The march of folly: from Troy to Vietnam

David Fromkin
   The Way of the World: From the Dawn of Civilizations to the Eve of
   the Twenty-first Century

Margaret MacMillan
   War: How Conflict Shaped Us

   Dangerous Games: The Uses and Abuses of History

Yuval Noah Harari
   Sapiens: A Brief History of Humankind

Mary Beard
   S.P.Q.R. A History of Ancient Rome

Jared Diamond
   Collapse

David Graeber and David Wengrow
   The Dawn of Everything: A New History of Humanity

Steven Pinker
   The Better Angels of Our Nature

Leonard Mlodinow
   The Upright Thinker

Kai Bird and Martin J. Sherwin
   American Prometheus: The Triumph and Tragedy of J. Robert Oppenheimer

Politics
--------

Ibram X. Kendi
   How to Be an Antiracist

Francis Fukuyama
   The Origins of Political Order: From prehuman times to the French
   Revolution

Michael Lewis
   The Fifth Risk

Jae Gutterman
   Her Neighbor's Wife

Kurt Kohlstedt and Roman Mars
   The 99% Invisible City: A Field Guide to the Hidden World of
   Everyday Design

John Green
   The Anthropocene Revisited: Essays on a Human-Centered Planet

Art
---

E\. H. Gombrich
   | The Story of Art
   | (There is an edition of this book for younger readers.)

   Art and Illusion: A Study in the Psychology of Pictorial
   Representation

Miscellaneous
-------------

Hanif Abdurraqib
   They can't kill us until they kill us: essays

Garry Kasparov
   How Life Imitates Chess

Computer science, AI, math
--------------------------

Melanie Mitchell
   Artificial Intelligence for Thinking Humans

   Complexity: A Guided Tour

Douglas Hofstadter:
   Godel, Escher, Bach: An Eternal Golden Braid - A metaphorical fugue
   on minds and machines in the spirit of Lewis Carroll

Julia Galef
   The Scout Mindset: Why Some People See Things Clearly and Others Don't

Steven Pinker
   Enlightenment Now: The Case for Reason, Science, Humanism, and
   Progress

   Rationality: What It Is, Why It Seems Scarce, Why It Matters

Leonard Mlodinow
   The Drunkard's Walk

Richard Hamming
   The Art of Doing Science and Engineering: Learning to Learn

Nate Silver
   The Signal and the Noise

Brian Christian and Tom Griffiths
   Algorithms to Live By: The Computer Science of Human Decisions

Psychology
----------

Michael Lewis
   The Undoing Project

Jim Davies
   Riveted: The Science of Why Jokes Make Us Laugh, Movies Make Us
   Cry, and Religion Makes Us Feel One With the Universe

   Being The Person Your Dog Thinks You Are: The Science of a Better
   You

Charles Duhigg
   The Power of Habit: Why We Do What We Do in Life and Business

David McRaney
   You are Not so Smart

Marcel Mauss
   The Gift

Writing and literary criticism
------------------------------

Harold Bloom
   The Western Canon: The Books and School of the Ages
   (
   https://www.kirkusreviews.com/book-reviews/harold-bloom/the-western-canon/ )

   How to Read and Why

Sandra M. Gilbert and Susan Gubar
   The madwoman in the attic : the woman writer and the
   nineteenth-century literary imagination

Steven Pinker
   The Sense of Style: The Thinking Person's Guide to Writing in the
   21st Century


Philosophy
----------

Anthony Gottlieb
   The Dream of Reason: A History of Philosophy from the Greeks to the
   Renaissance

   The Dream of Enlightenment: The Rise of Modern Philosophy

Bertrand Russell
   A history of Western Philosophy

Alasdair MacIntyre
   After Virtue: A Study in Moral Theory

Jonathan Glover
   Humanity: A Moral History of the Twentieth Century

Carlo Rovelli
   Reality Is Not What It Seems: The Journey to Quantum Gravity

Brian Christian
   The Most Human Human: What Artificial Intelligence Teaches Us About
   Being Alive

   The Alignment Problem: Machine Learning and Human Values

Thomas Moore
   Utopia

Medicine and epidemiology
-------------------------

Laurie Garrett
   The Coming Plague

Richard Preston
   The Demon in the Freezer

Richard Rhodes
   Deadly Feasts

Jeremy N. Smith
   Epic Measures: One Doctor.  Seven Billion Patients.

Tracy Kidder
   Mountains Beyond Mountains: The Quest of Dr. Paul Farmer, a Man Who
   Would Cure The World

   Mountains Beyond Mountains (Adapted for Young People)


.. sidebar:: How to *frame* these resources

   Read critically: for each book or article you read, look for
   articles with contrasting points of view.  Always make sure you are
   convinced that you understand the context ... make sure you have
   always asked the next question (refer to our other chapters)
   ... does it teach us how to write about subjects?  ... FIXME:
   unfinished


Articles, article collections, blogs
====================================

Paul N. Edwards
   How to Read a Book (version 5.0 on 2022-05-29) -
   https://pne.people.si.umich.edu/PDF/howtoread.pdf
   archived at:
   https://web.archive.org/web/20230526182410/https://pne.people.si.umich.edu/PDF/howtoread.pdf

Articles related to the New York Times "1619 Project"
   * https://www.nytimes.com/interactive/2019/08/14/magazine/black-history-american-democracy.html
   * https://www.nytimes.com/2019/12/20/magazine/we-respond-to-the-historians-who-critiqued-the-1619-project.html
   * https://www.theatlantic.com/ideas/archive/2019/12/historians-clash-1619-project/604093/
   * https://www.theatlantic.com/ideas/archive/2020/01/inclusive-case-1776-not-1619/604435/

Collection of articles on world history
   https://www.worldhistory.org/

The Manhattan Project: an interactive history
   https://www.osti.gov/opennet/manhattan-project-history/index.htm

Voices of the Manhattan Project
   https://www.manhattanprojectvoices.org/

Sridhar Mahadevan
   Quora response on art and AI - https://www.quora.com/Do-you-believe-human-art-and-design-is-about-to-crumble-because-of-the-introduction-of-artificial-intelligence/answer/Sridhar-Mahadevan-6?share=1

Taylor Branch
   The Shame of College Sports -
   https://www.theatlantic.com/magazine/archive/2011/10/the-shame-of-college-sports/308643/

Blas Moros's The Rabbit Hole
   blog and more - https://blas.com/

You are Not so Smart
   blog - https://youarenotsosmart.com/

Brian Doyle
   How Did You Become a Writer? -
   https://theamericanscholar.org/how-did-you-become-a-writer/


Audio/visual
============

This section is necessarily less permanent.  The links I give here
might not work: some material might have disappeared from the web, or
might have been moved to a different location.  Still, it should be
possible to find almost all this material with some effort.

Lectures
--------

Richard Feynman
   Los Alamos From Below - https://www.youtube.com/watch?v=uY-u1qyRM5w

Richard Rhodes
   Twilight of the Bombs - https://www.youtube.com/watch?v=IRN2g8uoQkg

Stuart Firestein
   The Pursuite of Ignorance - TED talk -
   http://historyofliterature.com/

   Ignorance, Failure, Uncertainty, and the Optimism of Science -
   Santa Fe Institute public lecture -
   https://www.youtube.com/watch?v=YIah2JtqlZk

Melanie Mitchell
   Artificial Intelligence: A Guide for Thinking Humans -
   https://www.youtube.com/watch?v=NMUqvhuDZtQ&t=228s

   The Future of Artificial Intelligence -
   https://www.youtube.com/watch?v=GwHDAfAAKd4

Brian Arthur
   Complexity Economics - https://www.youtube.com/watch?v=P8IzaECeQOk

Paul Bloom
   How Pleasure Works - https://www.youtube.com/watch?v=lWOfP-Lubuw

   Can prejudice ever be a good thing? -
   https://www.youtube.com/watch?v=KDBcoRLkut8

Amanda Palmer
   The Art of Asking - https://www.youtube.com/watch?v=xMj_P_6H69g

Brian Christian
   The Most Human Human: What Artificial Intelligence Teaches Us About
   Being Alive - https://www.youtube.com/watch?v=8Zs-GQ-ECLs

Quentin Skinner
   Machiavelli: A Very Short Introduction - Talks at Google -
   https://www.youtube.com/watch?v=CKGuzJ6GwHM

Richard Hamming
   You and Your Research - https://www.youtube.com/watch?v=a1zDuOPkMSw

Hans Rosling
   Global population growth, box by box -
   https://www.youtube.com/watch?v=fTznEIZRkLg

   DON'T PANIC - Hans Rosling showing the facts about population -
   https://www.youtube.com/watch?v=FACK2knC08E

   200 years in 4 minutes -
   https://www.youtube.com/watch?v=Z8t4k0Q8e8Y

David Foster Wallace
   This is Water - transcript and audio -
   https://fs.blog/david-foster-wallace-this-is-water/

Interviews
----------

Michael Lewis
   "In conversation" on the art of writing -
   https://www.youtube.com/watch?v=LVMW__W6ulY

Podcasts
--------

History of Literature
   http://historyofliterature.com/

Freakonomics
   https://freakonomics.com/podcast/

Alex and Books
   https://alexandbooks.com/podcast

Malcolm Gladwell's Revisionist History
   https://www.pushkin.fm/podcasts/revisionist-history

Hari Kunzru's Into the Zone
   https://www.pushkin.fm/podcasts/into-the-zone

The BBC's Start the Week
   https://www.bbc.co.uk/programmes/b006r9xr/episodes/downloads

Melvyn Bragg’s In Our Time
   https://www.bbc.co.uk/programmes/b006qykl

Roman Mars
   Roman Mars Can Learn About Con Law - https://learnconlaw.com/

   99% invisible - https://99percentinvisible.org/

Strong Songs
   https://strongsongspodcast.com


... And what about fiction?
===========================

Fiction might have greater influence on us than non-fiction: reading
fiction alters our mood more, which can predispose us to assimilate
new ways of experiencing subject matter.  This can lead to provoking
new thoughts that then stay with us.  Fiction, as well as art and
music, can also make us realize that the words and art media are
pliable and can be formed into remarkable new works.  That frame of
mind would be a precious one for a researcher.

But is there a reason to put a list here?  I thought about it at
length: it is tempting to mention the books that have had great effect
on me, or that I know have had the effect on others.  But it would not
put *you* along a path to get the same out of it.

I do not know how to work in with the indirectness with which fiction
affects us, which ...  in the end I came up with science fiction
books, but they seemed appropriate from a different point of view.
The extreme "world building" in science fiction gives it a sliver of
non-fiction-ness.

So I am looking for suggestions on how to think about a section on
fiction, or on reasons to remove this stub altogether.  Meanwhile I
have two suggestions:

* Follow reading lists from books of literary criticism, or the
  light-weight version which are blogs and podcasts about books.  Now
  your reading of fiction will be accompanied by an analysis of the
  books, and you will have a greater self-awareness of the effect it
  has had on you.

* Read treatises that discuss a genre of fiction on a large-scale,
  such as Aristotle's "Poetics", Auerbach's "Mimesis", Nietzsche's
  "The Birth of Tragedy", ...


.. rubric:: notes on future additions

Zora Neale Hurston - maybe "Fannie Hurst"

Richard Dawkins (books and TED talk)

Steven Jay Gould
