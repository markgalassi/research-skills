========================================
 Nuance and the Avoidance of Platitudes
========================================

How To See More of the Painting
===============================

Imagine you're looking at a painting. Maybe you're standing at a distance and
you have a beautiful perspective of the painting's composition. Maybe you're
standing as close as possible and you can see the detail and mastery in the
artist's brush strokes and color choices.

While both of these perspectives give you insight into the image and are
enjoyable in their own right, neither of them fully encapsulate the painting
as a whole. In order to truly understand the painting, you must look from a distance
and up close. So too with academic topics.

Part of critical thinking is seeing the nuance in any topic we learn about. Upon
initial observation, we only ever see a portion of any picture. In order to be
good researchers, and practice good critical thinking, it's crucial that we
understand that topics must be closely examined in their individual detail, and
also in the context of their field. The difference between academic topics and
paintings is that researching the real world never really gives you a nice frame
to bound your topic. Our world is so vast and interconnected that we can never
zoom out far enough, and at the same time understand with enough detail, that
our view of the picture can really be called "complete". Nuance and critical
thinking involves looking beyond the surface-level or simple explanations, and
knowing that we'll never know everything there is to know. It's about
understanding that things are never black or white, but instead are shades of
gray.

For example, the `Zoom comic strip by Istvan Banyai (linked here)
<http://web.archive.org/web/20240518221558if_/https://3.bp.blogspot.com/-wblutbx_F0w/UnGTPz1futI/AAAAAAAAGLs/4VrJMrEmB7E/s1600/zoom.jpg>`_
shows a few images that change dramatically as the scope and
perspective changes:

Notice how the images themselves are not altered, only the perspective
of the viewer is.

The observer at the art museum can walk around the painting in order
to gain perspective, but how can we as academic researchers do the
same?

The easiest place to start is by recognizing limitations.
Understanding nuance involves recognizing that no single source or
study perfectly encapsulates an entire topic or field.

Some also call this *triangulation in research*: multiple research
methods are used to validate findings.  Just as the more places the
art museum goer stands in the room the better their understanding of
the painting becomes, the same goes for research findings.

Another wonderful way to address nuance is by discussion.  Among peers
or professionals, discussing a topic will give you, literally and
figuratively, as many perspectives as possible. However, discussion
requires other people, and often people will avoid having to discuss
nuances or approach disagreements by using platitudes.

Another metaphor we find useful when thinking about gaining multiple
perspectives on a topic is the practice of tomography.  A famous
example of optical tomography is from a researcher from Portugal,
Gabriel Martins, who recreated a 3D construction of an embryo by
compiling over 1,000 images.  You should approach research similarly:
in order to gain a fuller understand of a topic as a whole, take many
pictures from different angles, and let your mind compose a more
nuanced picture (analogous to a three-dimensional image).

Platitudes
==========

A killer of nuance is the habit of using platitudes. Sports
journalists, managers in businesses and other administration tasks,
trade magazine writers, all often fall in to this practice.

A platitude is an overused statement that might sound deep or meaningful,
but often lacks real significance. It's a cliche, it's vague, and it lacks
substance.

Platitudes are so detrimental to the understand of nuance because they
hinder meaningful discussions by catering to superficiality rather
than real depth.  They frequently leave out or oversimplify complex
issues into brief sound bites.

To explain further, we here reproduce, with permission, a brief essay
by Jerry Silfer who is a public relations consultant and blogger. It
makes this point nicely.

The Platitude Sickness: Taking Out the Trash of Corporate Speak
---------------------------------------------------------------

:Author: **Jerry Silfwer**
:Source: https://doctorspin.org/creative/storytelling-writing/platitude-sickness/

I sometimes hate what I do for a living.

A sizable portion of what I write for clients will pass through
numerous of filters before getting published. And the end result is
nothing but a dwindling tirade of cringy corporate platitudes.

I’m not alone in feeling this way. We’re all exposed to corporate
speak. Whether you’re in marketing and communications or not, you’ll
see these platitudes everywhere. And for some reason, platitudes are
becoming the go-to format for many branded content strategies.

.. pull-quote::

   .. rubric:: According to Wikipedia:

   "A platitude is a trite, meaningless, or prosaic statement,
   generally directed at quelling social, emotional, or cognitive
   unease. The word derives from plat, the French word for “flat.”
   Platitudes are geared towards presenting a shallow, unifying wisdom
   over a difficult topic. However, they are too overused and general
   to be anything more than undirected statements with an ultimately
   little meaningful contribution towards a solution."

Corporate platitudes are such a waste of editorial
space. Unfortunately, the platitude sickness tends to do quite well in
social media.

A text loaded with obvious statements and no real knowledge can still
attract quite a lot of social engagement. People often hit that “Like”
button (or emoji-button or whatever) without even reading the actual
article it refers to.

"It’s important to have a strategy."

"Always put the customer first."

"Be proactive and think long-term."

"Publish epic content."

Their engagement reflects how they agree with the headline and how it
adds to their own personal worldview. It’s probably also a
psychological bandwagon-effect at play, a way of signal belonging to
important social circles.

So, how can you combat the platitude sickness in your corporate
communication?

.. pull-quote::

   Make it your personal mission to find platitudes and to destroy
   them. As this becomes a ritual, you’ll develop an “allergy” to
   corporate platitudes — and removing them will become second nature.

Welcome to the fight — I’m happy to have you onboard.


Another Rant Against Platitudes
-------------------------------

If you need further convincing, `The Speech Dudes
<https://speechdudes.wordpress.com/2015/10/20/politics-and-platitudes-the-no-shit-sherlock-test/>`_
have a strongly worded rant about the waste of life that platitudes
are. If nothing else, the amount of literature and articles on the
nuisances of platitudes should demonstrate to you not only how problematic
they are, but also just how prevelant. 

They first comment:

.. pull-quote::

   More often than not, a platitude simply states the obvious and so
   would be better off not having been uttered [1]

and footnote [1] is:

.. pull-quote::

   [1] With a platitude, not only is there a stating of the obvious
   but it’s also done in such as way as to have the appearance of
   being profound or wise. Facebook is full of such pre-digested
   pabulum that, sadly, spreads like linguistic herpes, passed on by
   well-meaning but ultimately uncritical people who think that
   quoting something that sounds smart also makes them sound smart. It
   doesn’t. Platitudes also seem to aspire to taking on a moral
   dimension, presumably to reinforce the semblance of profundity.

After personally insulting much of the population that posts on
Facebook, a social media platform popular among middle-aged and
elderly people, they go on to mention corporate *mission statement*
platitudes:

.. pull-quote::

   A mission statement such as "To combine aggressive strategic
   marketing with quality products and services at competitive prices
   to provide the best value insurance for consumers" is about as
   broad as you can get [... this really is the mission statement of a
   large insurance company]

They finally bring in a criminal organization as an example:

.. pull-quote::

   Here’s one you might have heard some years ago: Respect, integrity,
   communication, and excellence. This was from the company called
   Enron, which was one of the most notorious business scandals in
   American history and is considered by many historians and
   economists alike to be the unofficial blueprint for a case study on
   White Collar Crime. It’s also an example of a crime against
   vocabulary for creating such a miserably loose mission statement.

Clearly the Speech Dudes have an axe to grind here, but I find myself
in agreement that every example they give is awful.

As an exercise, you could visit `a rather sad site dedicated to
mission statements
<https://www.missionstatements.com/fortune_500_mission_statements.html>`_,
and go to the Fortune 500 Mission Statements page
[Fortune500MissionStatements]_ and pick out the few cases in which the
company's mission statement made you think "aha! this is original,
bold, and memorable".  Consider, "what makes this better than the
others?" - "What are they saying that is unique and meaningful?" -
"How did they say it in a way that avoided platitudes?"
   

Mark's Personal Experience
--------------------------

I have worked at Los Alamos National Laboraotry for my entire career.
Los Alamos started during the second world war, a group of scientists
came together to design and build the first atomic bomb.

Later in its history, Los Alamos developed a more corporate atmosphere,
with layers of management and an administrative manual with tens of
thousands of pages.

As I was writing this section (May 2021) I looked up what our mission
statement was, and found:

.. pull-quote::

   Los Alamos National Laboratory's mission is to solve national
   security challenges through scientific excellence.

It surprised me for not being awful -- maybe just a bit too long.
The mission statement from earlier in my career (1994), formulated by
former Los Alamos director Sig Hecker, had been stronger, bolder, and
more challenging:

.. pull-quote::

   Reduce the Global Nuclear Danger.
   [Hecker1994]_

Simple, direct, and packing a punch.
