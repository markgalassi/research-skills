===============================
 Products - written and verbal
===============================

The Written
===========

Here I will just put it very concisely: you need to be ready to write
a lot.

Don't let your perceptions of traditional scientific writing influence you too
much because:

   Almost all scientific and technical writing is lousy.

Most of the material here is in the slides that Mark Galassi gives in
his presentation on writing.  I still need to add the information to
this chapter.

But I will put exercises here.

The thing being shown
---------------------

For the mantra:

   Focus on *the thing being shown*, not on *the activity of studying
   it.*

we use this example:

.. note::

   In recent years, an increasing number of researchers have turned
   their attention to the problem of child language acquisition. In
   this article, recent theories of this process will be reviewed.

Every student should now open up a text file and write their own
replacement, and put it into the chat.  We can then refer to Pinker's
example alternative as well as all the students.

Then we return to the slide to see Pinker's alternative phrasing.


Legal writing
-------------

.. note::

   "I have serious doubts that trying to amend the Constitution
   ... would work on an actual level.... On the aspirational level,
   however, a constitutional-amendment strategy may be more
   valuable."

Every student should now open up a text file and write their own
replacement, and put it into the chat.

Also:

.. note::

   "It is important to approach this subject from a variety of
   strategies, including mental health assistance but also from a law
   enforcement perspective."

Every student should now open up a text file and write their own
replacement, and put it into the chat.

Then we return to the slide to see Pinker's alternative phrasing.


Nominalizations and "zombie nouns"
----------------------------------

::

   appear -> make an appearance
   organize -> bring about the organization of

.. note::

   The proliferation of nominalizations in a discursive formation may
   be an indication of a tendency toward pomposity and abstraction.

Every student should now open up a text file and write their own
replacement, and put it into the chat.

Academic/business writing
-------------------------

.. note::

   Prevention of neurogenesis diminished social avoidance.

Every student should now open up a text file and write their own
replacement, and put it into the chat.

.. note::

   Subjects were tested under conditions of good to excellent acoustic
   isolation.

Every student should now open up a text file and write their own
replacement, and put it into the chat.

.. note::

   I’m a digital and social-media strategist. I deliver programs,
   products, and strategies to our corporate clients across the
   spectrum of communications functions.  Then we return to the slide
   to see Pinker's alternative phrasing.

Every student should now open up a text file and write their own
replacement, and put it into the chat.

.. note::

   [on a generator's warning label] Mild Exposure to CO can result in
   accumulated damage over time. Extreme exposure to CO may rapidly be
   fatal without producing significant warning symptoms.


The Presented
=============

You should repeat this mantra frequently:

.. note::

   Almost all presentations are lousy.

That means that your advice and examples should come from people who
are aware of this and are trying an approach to give non-lousy
presentations.

Problems to Overcome
--------------------

Here are some frequent issues with presentations, and our attempt at
guidance in overcoming them.

Mechanical -- technology
~~~~~~~~~~~~~~~~~~~~~~~~

Missing cable adaptors, fonts in slide show files, embedded videos, ...

Mechanical -- slides
~~~~~~~~~~~~~~~~~~~~

Slides that are bullet lists, or have large chunks of text, are
usually awful.


Content
~~~~~~~

The talk is not that interesting because the content is not good.  The
speaker does not motivate the subject matter.

Confusion
~~~~~~~~~

The lack of a clear plan, so it later feels like it was a jumble of
information, with no take-home points.

Dynamics
~~~~~~~~

The speaker's tone doesn't work.  Not all speakers are lively, but
that's OK: A deadpan speaker can deliver very compelling talks.  So
the problem is a nuanced one, usually boiling down to the fact that
the speaker has not yet found a good synthesis of their speaking style
and the way they organize the material.

Another problem that comes up quite frequently, especially in younger
presenters, is that of speaking too quickly.



The Solution - a reasonable baseline
------------------------------------

Mechanical -- technology
~~~~~~~~~~~~~~~~~~~~~~~~

Show up early and do a "tech check" ahead of time.

Mechanical -- slides
~~~~~~~~~~~~~~~~~~~~

The TED organizers have thought about this matter carefully and have
come up with good guidelines on preparing slides.

https://www.ted.com/participate/organize-a-local-tedx-event/tedx-organizer-guide/speakers-program/prepare-your-speaker/create-prepare-slides

Following their guidlines on images and text-heaviness will solve most
of the mechanical problems.  My brief summary of those points is that
*you* tell the story with *your voice*; the slides just provide visual
aids and props.  You *never* read off slides.

Solving these mechanical problems with slides will have a cascade
effect that will help solve some other shortcomings as well.

Some more advice from the TED people is at:

https://ideas.ted.com/6-dos-and-donts-for-next-level-slides-from-a-ted-presentation-expert/

In this post they show (item #4) a specific example of going from a
first pass with busy graphs to a final graph that is much easier on
the audience.

You also get a significant improvement with almost no work by simply
shifting your slide aspect ratio from 4x3 to 16x9.  Make this setting
the default, and you will find it hard to go back.

Keeping Your Audience Engaged
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now for the next step toward really gripping your audience.  I will
give you some prescriptions.  You should not take them as absolute (they
are my style, after all, not necessarily yours), but rather see how
they mix with your own style, and if they are useful.

For some of these I have links to some talks, or articles about talks.

Motivation: Personal and Global
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Always remember that **motivation is everything**.  Repeat this to
yourself again and again.  You start out by giving the motivation for
the work you are presenting.

I suggest offering motivation separated out into two categories:

#. Your personal motivation - this is where you tell them why you are
   fascinated by something, what drove you in to this.  Keep it brief,
   but do bring it up: it is part of how you charm an audience into
   following you more closely.

#. The "deep in the bones of the world" motivation: this is where you
   reassure the audience that you and your colleagues understand the
   nature of the world, and how your research subject fits into it.
   If you are talking about a narrow result, you still connect it to
   one of the great themes that drive history.

The second type relates back to the "attitude of the grizzled veteran"
suggestion above.  Audiences like it when you project that you speak
from that depth.  The psychological factor is huge: when our
up-and-coming generation of young researchers (like you) project a
smooth calm competence, audiences really get a lot out of the talk.
(Of course part of this is due to the fact that your big-talk
subconsciously justifies the time they devote to your talk - but "take
the win": it means they will pay attention and remember more.)

Cognitive Basics: Don't Tire Your Listeners
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Listening to long winded stories, or reading certain materials, forces the
listener or reader to do a lot of work to understand how it comes
together.

Don't do that to them.

Your lecture should not feel like watching the Matrix Movie for the
first time; confusing until a brief moment of clarity at the end.

Attitude: The Grizzled Veteran
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here's a tip I give people when they give talks: talk like you're a
grizzled old-timer in that field.  Have fun telling stories and
presenting the "insider" view.

This also applies when people are 16 years old: they have experiences
and they can present them like veterans.

For example, let's say you are talking about the C programming
language, developed between 1972 and 1973 by Dennis Ritchie at Bell
Labs.

You could simply present the facts and say

   "C was developed by Dennis Ritchie at Bell Labs between 1972 and
   1973".

Or you could add color as if you were in the room when it happened,
and add:

   "At the time FORTRAN was the most widely used programming language,
   but it was only effective for scientific computation and not for
   systems programming.  Back then systems programming was almost
   entirely done in assembly language, a very detailed language which
   maps directly [and here you are using hand gestures to drive your
   point home] to the machine codes.  Few people are happy writing big
   programs in assembly.  Rithcie's genius was to design a language
   that was good for both *low level* programming (you really know
   what's happening in the hardware when your C program runs) and high
   level programming (you have high level features like structures).
   C adoption spread rapidly and Ken Thompson and Dennis Ritchie
   re-wrote the early UNIX operating system in C."

As you tell the story you chuckle and express the fun that an
old-timer would have remembering those days when C came onto the
scene.

You could then continue by saying that in the 1980s and 1990s C became
the choice language for high performance numerical computing,
gradually overtaking FORTRAN (although FORTRAN still retains an
enduring market share).

And of course that's all your story-telling: it's not on the slides.
You have been pacing the room and using hand gestures to tell this
story.

As an example, those familiar with old Rock and Roll music might
remember the song "Sympathy for the Devil", by the Rolling Stones.
The character narrating that song in first person is the ultimate
grizzled veteran.

So why would you do all this?  Audiences want to feel that they are
listening to a key person in the field, someone who was part of the
story they are telling (even if only because they studied it).  Giving
them that feeling makes them enjoy the talk more, be less fatigued,
and learn better.

Having said all this, remember: this is a course on critical thinking,
so let us look at criticisms.  There are, indeed, valid criticisms to
the attitude of the grizzled veteran.  One is:

* It is frivolous and wastes time.  A solution: make sure that after
  every one of those anecdotes you return to a serious part of your
  project that drives the science forward.  Ramble a bit, but then
  show that you are a no-nonsense researcher pushing forward, not
  someone trapped in nostalgia.

For the sake of rigor, and to keep up our habit of being hard-nosed
and brutally honest in our reporting, I will add some crucial
requirements for the speaker who wants to come across like a grizzled
veteran:

* You must have really studied the material so well that you can tell
  the stories authoritatively.  Audiences spot that feeling of depth
  and they understand when you have assimilated the story, rather than
  memorized the words in the story.

* You must be accurate.

* You must calculate carefully that the stories will give new
  *insight* to your audience - otherwise they are indeed a waste of
  time.  The story of Dennis Ritchie shows how this remarkable
  innovation came out of a crisis that needed a solution, and also
  paints the picture of a thriving dynamic team at Bell Labs in that
  period.

Another example: your discussion touches upon Dante.  You could say:

   "[...] Dante, the medieval Italian poet and writer, [...]"

Or, if you intuit that your audience barely knows who Dante was, you
could take the opportunity to add:

   "Here's how to visualize the intellectual landscape in Italy in the
   late 1200s: people had started moving from farmhouses and villages
   into cities again, reversing a trend that had started with the fall
   of the Roman Empire.  This means that there was a new intellectual
   pole: earlier the only scholars were monks in monasteries -- names
   you might have heard are Abelard and St. Thomas Aquinas.  Now, in
   the late 1200s you have city-dwelling intellectuals, often drawn
   from minor nobility or even the bourgeoisie.  Dante grew up in this
   environment in Florence, and he became a poet - by many accounts
   the greatest poet in the Western canon."



Humor: An Effective Crutch, but is it Useful?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are convincing reasons to use comedy in talks or discussions. It
can help keep the audience engaged and, though it does not suit all
presenters, it can be an effective tool in the toolkit of anyone who
communicates.

Humor has to be used carefully since the same joke is perceived
differently by different cultures, sub-cultures, and individuals.
Some solve this problem by having the humor come out in subtle ways,
like carefully pointing out an amusing irony in a situation.

In general I enjoy humorous talks, and remember their points better,
so *I personally* encourage humor in talks -- but remember what I said
above about the "old-timer storytelling" attitude, when I pointed out
the "it is frivolous and wastes time" criticism.  A solution is to
make sure that after a you take a humorous flight, you quickly return
to the serious discussion of the work.  This rapid return to serious
work creates a subliminal perception of momentum in the audience, and
they leave with a feeling that good and important things are
happening.

Here are a few lectures I have enjoyed, which highlight different ways
in which humor is used or not:

Tim Urban's "Inside the mind of a master procrastinator"
   https://www.youtube.com/watch?v=arj7oStGLkU
   Tim Urban's talk is almost entirely humorous, and while his
   take-away points are valid, the results are also to be seen in a
   humorous vein.

Paul Bloom's TED talk on the value of art
   https://www.youtube.com/watch?v=RPicL1AWrs8
   Paul Bloom's talk is a serious discussion of how the mind assigns
   value to things, but all the examples he gives are quite funny.

Julia Galef's discussion of the "hidden prior"
   https://www.youtube.com/watch?v=BrK7X_XlGB8&t=207s
   Julia Galef mentions a joke in passing, but mostly to make a point
   about people's perception of mathematicians.  For the rest her talk
   is serious and to-the-point.  I like it as much as others because
   she discusses an important point carefully.


Not Taking Yourself Too Seriously
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Related to the discussion of humor, here are some examples of very
carefully prepared *parodies* of the talks given by thought leaders:

Thought Leader
   https://www.youtube.com/watch?v=_ZBKX-6Gz6A

John Oliver's "Todd Talks"
   (Before choosing to watch this, remember that John Oliver uses some
   language intended for mature audiences.)
   https://www.youtube.com/watch?v=vvtp-dKfbco
   This is part of a longer segment:
   https://www.youtube.com/watch?v=0Rnq1NpHdmw&t=43s

Use these talks to remember that, even though you want to present
yourself as a "thought leader", you should not take yourself too
seriously.


A Couple of Pitfalls for the Younger Crowd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Younger researchers will often put "**I would like to thank** my
mentor AAA BBB, and my teacher CCC DDD [and sometimes even a parent]".
I recommend against putting it that way: you are not the center of
this story - the science is.

There are a few ways in which professional researchers give credit to
their collaborators.  It's usually toward the start of the talk, or at
an appropriate moment during the talk.  It is phrased as "... this
work was done in collaboration with ...", or "... the foundational
work for this was done ..."

I recommend thinking of the two or three direct influences you have
had, and mentioning their *specific role* in the acknowledgments.
This might or might not be your mentor.  I'm sure you can find another
way to thank your mentor.

Another problem with presentations by young researchers is that they
**recite verbatim**, sometimes even **reading from a script**.

It is hard to imagine how anyone might have thought this is a good
idea: I have been to talks where people did that and I felt "why are
they wasting my time?"  But apparently it has been taught in some
schools as a valid way of giving a presentation.  I have even seen
people read a presentation from their phone.

Avoiding this requires a bit of work: you have to really learn your
material!  Of course if you're giving a talk on something, let us hope
that you know it cold.  But make that knowledge even more intimate:
spend time in your mind visualizing all the things you will discuss,
and probe all the possible related issues with your mind, and do a bit
of extra reading on those related issues.

Young researchers also **often speak very quickly** and finish their
talk after no time at all.  There are techniques for spacing it out.

For example, if you have a slide with a cool picture or quote or plot
(as you should), stop for a while, pace back and forth in front of it
(or have a sip of water if you are presenting in videocon) - you can
either mention what's cool and how you like looking at those graphs
and how to think about the axes, or you can just pause and let the
audience look at it.

Or you can dive into the background for something, as we mentioned
when discussing the "grizzled veteran" approach; you can also stop to
give motivation, possibly with a personal anecdote.

And **what if you are terrified?** Stage fright is a problem, but most
of the time you will find yourself getting over it by starting to talk
about your subject - do it right away.

Physicist extraordinaire Richard Feynman tells this story from when he
was in his early 20s, a graduate student at Princeton University.  His
advisor John Wheeler had him give a talk, and invited Wolfgang Pauli,
John von Newmann, and Albert Einstein to listen.  Feynman was quite
nervous, as he recounts in his memoir *Surely You're Joking,
Mr. Feynman:*

   Then the time came to give the talk, and here are these monster
   minds in front of me, waiting! My first technical talk -- and I
   have this audience! I mean they would put me through the wringer! I
   remember very clearly seeing my hands shaking as they were pulling
   out my notes from a brown envelope. But then a miracle occurred, as
   it has occurred again and again in my life, and it's very lucky for
   me: the moment I start to think about the physics, and have to
   concentrate on what I'm explaining, nothing else occupies my
   mind--I'm completely immune to being nervous. So after I started to
   go, I just didn't know who was in the room. I was only explaining
   this idea, that's all.
