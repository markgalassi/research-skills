=====================
 Reading Effectively
=====================

Lesson ideas for this chapter
=============================

This chapter, when taught in a tutorial at the Research Skills
Academy, should have frequent breaks for students to do exercises.

These can be exercises to read up on a given topic, and practice
their skill in quickly marshaling resources.

There are a couple of placeholders in the text recommending that the
class stop and take some time to do the exercise.

Reading to Understand Vs. Reading to Critique
=============================================

Whenever we take in information, especially complex information, we undergo
a battle between understanding that information on its own terms and reflecting
on how we judge and evaluate that information. If you sit down to read an
essay, you have to both make sure you understand what the essay is trying to
get at, as well as thinking critically about whether you think that essay is
right. "Critique" here doesn't just refer to our usual idea of being critical
or saying what we think is wrong (although this is definitely a form of
critique), but more broadly refers to how we react to the text. To be
good readers and researchers, we need to be able to critique in both senses 
of the word, and identify when we're failing to do one because we're too 
engrossed in the other.

Even though this section is titled "Reading Effectively," all of these
techniques apply to any kind of research or acquisition of new information.
Whether you're attending a lecture, watching a documentary, or learning
experientially out in the field or the lab, applying these ideas will help
you gain purchase on your topic of study and turn what you learn from a
series of facts into a nuanced understanding.

Understanding with Grace
------------------------

When we read things that are new to us, it's important to know that we're not
going to understand it all right away and to reserve judgement for a bit. One
concept I use often is to approach what I'm reading with grace, that is, to
assume that what I'm reading is written in good faith and is more or less
worthwhile. I might reevaluate those ideas later, but it keeps me from getting
prematurely frustrated at texts and helps me get the most I can out of them.

When reading W.E.B. Dubois' *The Souls of Black Folk*, I was really taken aback
by the idea of "the talented tenth," his concept that the top 10% of Black
people were fit for education and would lead issues of racial change. To me, 
this concept seemed elitist and almost had a tinge of eugenics to it. While I 
still don't agree with Dubois on the talented tenth, if I had let myself get 
frustrated by this idea and stopped reading, I would have missed out on ideas
of his that are really useful, like Double Consciousness. Even  though the
talented tenth isn't something I think is valid, there's a lot of  very
important ideas in civil rights that were written in response to it. I  would
have a much harder time approaching those ideas were I not already familiar
with the talented tenth.

The takeaway here is that in your research, you need to be able to remove
your own opinions and instant reactions and reserve judgement initially to make 
sure that you're getting everything you can get out of your sources. This is by
no means to say your opinions and reactions don't matter, or shouldn't be discussed,
but, by placing your initial reactions or modern sensibilities on the sidelines, 
you broaden the kinds of information and insights you'll be able to gain access to.

Critiquing with Wanton Abandon
------------------------------

It's not enough to make sure you understand a source you come across, you
also have to ask yourself how you're going to evaluate that source. Thinking
critically means putting the information you've gathered into context with 
your own thoughts and knowledge. This means asking questions like, "Do I trust
this source's author(s)?" "Is their methodology for arriving at their conclusions
sound?" or "What are the implications of believing this source?". When we start
critiquing our sources, we get an opportunity to put our own brain power
towards our research.

Despite the structure of this section, you shouldn't let yourself believe that
understanding strictly precedes critique or that critique strictly comes
afterwards. Often our critical process, where we try to figure out how to put
the information we've been given into a broader context, opens up new ideas that
help us understand the source better. It's a back and forth process.

Notes Are Your Best Friend
--------------------------

If you're just reading a couple short articles or essays on a topic, you might
be able to reasonably hold everything you learn in your head. However, as your
list of sources and your sources themselves get bigger, it'll become harder
and harder to keep track of it all on your own. Taking good notes is paramount
to making sure that you're able to not only retain what you learn, but that
you're able to conveniently access it later.

It's hard to give exact advice when it comes to note taking. Note taking
strategies are very personal, and you have to find something that works for
you. You should always be critically refining your note taking strategies and
figuring out where they can better serve you.

First, you should pay attention to what sorts of things you tend to write down,
and what sorts of things you tend to reference. Do you find yourself constantly
looking back to your notes to find definitions of terms that are important to
your research topic? Maybe highlighting definitions in your notes would make
them a better resource for you.

One thing that I find helpful is using bullet points in my notes to
understand and summarize what I'm reading, and I use asterisks to note where I'm
keeping track of my critical thoughts. This practice allows me to have a clear
overview of key points made in the text without having to flip through the
source. However, other people I know take all of their notes exclusively in
the margins of the source text, highlighting quotes and cross referencing
passages. This practice works well if you regularly cite quotes, or enjoy
deep analysis of texts.

It's helpful to experiment with different media as you refine your
note-taking practice. You might work best with a physical notebook, or
maybe you'll find that taking notes on a computer is more natural to you.
Maybe you do best taking notes in a minimal, distraction free text editor.
Maybe lots of organizational features from a larger piece of note taking
software are really useful to you. Maybe you do well with hierarchical
outlines, or scattered bulletpoints. Trial and error is the best way to
discover what system works for you, and keep in mind that your system
might very well vary based on the subject.


Reading non-fiction versus reading fiction
==========================================

Here simply refer to the section "Thought-provoking books and media",
in particular to the sidebar on "How to *read* these resources" and
take a moment there for everyone to read the sidebar.

This prepares us for the upcoming sections.

Handling Jargon
===============

We are often held back from feeling confident in our understanding
of a topic because of new jargon. Terms we don't recognize can make
us throw up our hands and say: "There is no way I can ever understand this."

The situation worsens when we see cocky young people or grizzled older people who
rattle off unfamiliar terms from psychology, electronics, chemistry, software,
or any other field. We might think; "Other people get this; why don't I?" These
hot-shots will happily talk about RAM, DIMMS, and affect (as a noun), and
Bayesan priors, and FMRIs, and conditional probability, and Brownian
motion, and van der Waals forces, and rear differentials, and carburetors,
and op-amps, and TTL versus CMOS, and FETs, and P-N junctions, and Shannon
Entropy, and round robin process scheduling, and, and, and ...

Until, eventually, we find ourselves completely lost in a forest of words
we don't recognize unable to see the path out. 

The truth is that terminology is just a superficial part of a topic.
The person who walks in to electronics class and has no trouble
talking about resistors, and capacitors, and inductors might know the
jargon but probably does not know how to actually *calculate* what
happens in that circuit, or how to match it with others.

You and the rest of the class will all be learning that material
together, and their head-start will be slight, not overwhelming.
Knowing the jargon might give one a language to talk about the subject,
but it does not neseccarily demonstrate extensive knowledge of the subject. 

Suspension of Not-getting-it-ness
---------------------------------

In theater, there is a concept called "suspension of disbelief."
This idea dates back to Aristotle, Horace, and Cicero, and the fortunate
wording was formulated by Samuel Coleridge.

We willingly suspend our disbelief to enjoy a play at the theater,
or a movie, or any work of fiction: we ignore the dissonance and
grasp the parts that matter. We don't see a dramatic fight scene
and constantly say: "That's not real! This is fake!" While we might
occasionally think that, especially if it is executed poorly, we
typically don't make a habit of it. For if we were to do so,
it would ruin out suspension of disbelief and at some level make
the entertainment useless. 

Just as you would use "suspension of disbelief" to enjoy the theater, you
can use the "suspension of not-getting-it-ness" to avoid getting weighed down
by jargon. If I run in to this phrase:

.. pull-quote::

   The popularity of the op amp as a building block in analog
   circuits is due to its versatility.
   (from the [WikipediaOpAmp]_ Wikipedia article on Op Amps)

When it comes to your approach, you have some choices as you read this sentence.
Here are three, and there could be others.

   a) I do not know the expression "analog circuits", so I will stop
      what I am doing until I have learned what analog circuits are.

   b) I don't understand one or a few of these terms so I am give up on
      reading the article.

   c) I can tell myself that some circuits are called "analog
      circuits" (some time in the future I will look that up), and
      they can be enhanced by using something called an "op amp",
      which I'm guessing is a clever piece of electronics.

All three are valid, but option C is most often the best use of your
time. As you read further, you might decide you need to learn more about
analog circuits, or you might find that the context of the article
taught you as much as you needed. It' also possible that you might discover
that the article itself is a side show to a larger topic, or, that the
discussion of analog circuits is a side show. Often articles used for research
are used by professionals to talk amongst other professionals, so it
very well may be that that material is not needed to understand the topic at
large, or even just the topic of your research. Option A gets in your way by
making you go down a side path on something that might end up being irrelevant.
If you keep reading and realize that you're not going to understand
your focus topic until you understand op amps better, then maybe do some
research. However, you might be able to save yourself a lot of time by trying
option C first. Option B makes you give up prematurely when it's quite
possible that you would have understood the article anyway, or at the 
very least assured yourself that the article was irrelevant to your research topic.

Your approach to learning terminology should be one of letting the terms wash
over you in a relaxed and flexible manner. Picture a sushi conveyor belt, or a baggage
carousel at the airport. You can watch as different terms pass by you, observing
the ones you don't recognize, and picking up which ones you want to eat, I mean,
investigate further. Some terms might stick quickly, others only when you
spend more time on them, but you should never feel that you do not belong. At
some point, even the most brilliant of minds were in your shoes. Jargon, like
any other skill, comes with time and exposure. 

Focus on the essence of what you are looking up, rather than getting
stuck when you see a new term. As you read or listen, flag new
terms to look up later, and see how far you can keep reading
without that term.

The Dream of Full Understanding?
--------------------------------

You should not feel guilt, or not feel that you are "copping out",
when you suspend your not-getting-it-ness.

The feeling of copping out might come from the fact that back in an
earlier, more innocent, part of your academic career you were able to
really understand the underpinnings of everything you learned. Now, as you
advance, we say to you: "Abandon that path!"

The last polymath, if polymaths even ever existed, would have lived a
very, very, long time ago. It has not been possible to learn and understand
everything for a long time now.

The advent of computer software, unlimited storage, the world wide
web, and all the accumulated knowledge that has come in its wake,
have made the world's body of knowledge so complex that it is not
possible to understand it all.

Just as you admit that you read a book without knowing the
chemistry of ink, and you ride in a car without having calculated the
heat dissipation from the cylinders yourself, you can also put up
with not fully understanding the entire collection of tools that you
use. You will want to do the best you can to "get the big picture"
of what you're reading, without getting stuck on jargon and details.

Remembering this when *you* are the writer
------------------------------------------

The goal is *not* to write without jargon: that has its own set of
problems.  Indeed, the replacement for jargon is often a collection of
heavy phrases that get in the way of the reader's flow.  Jargon was
often introduced for good reasons, so rather than abandoning it we
should shepherd it carefully.

One goal should be to either explain the jargon, or to craft your
phrasing carefully so that its meaning becomes clear just from the
reading of the text.  The other goal is to not introduce extra jargon
from other fields.  This comes up a lot with the use of acronyms.
Acronyms are almost always specific to a certain field such as
business or engineering.  You are not helping your readers by using
them.  Often you can just write out the entire expression the first
time, followed by the acronym in parentheses. A neat trick is to
occasionally mix up your phrasing of the name the acronym represents.

To give an example: in the United States you could say FBI or CIA, and
readers would probably not mind.  But unless you are writing for a
*very* select audience, you should not drop the expression MOSFET
(which stands for "metal-oxide-semiconductor field-effect-transistor")
casually.  You probably don't want to specify that level of detail at
all, but if you do want to drop it into a phrase, you could give the
full expansion of the initials.

.. pull-quote::

   For example, a phrase could look like "In 1947 researchers at Bell
   Labs had invented the *field effect transistor* - a small device
   that allowed an electric field to control the flow of current
   through a circuit.  This made the field effect transistor,
   abbreviated as FET, capable of being both an amplifier or a switch.

   In 1959, again at Bell Labs, this design was improved by the
   invention of a smaller and cheaper device called the "metal oxide
   semiconductor field effect transistor".  This new device,
   abbreviated as MOSFET, used sandwiched layers of metal, oxide, is
   the basis of modern digital (computers, ...) and analog (radio,
   stereos, ...) electronics.

An Exercise
===========

I mentioned Aristotle, Horace, Cicero, and Coleridge in section
"Suspension of Not-Getting-Itness." Pretend you have not heard of
some, or all, of these writers and try reading this section anyway.
Your mind could assimilate it like this:

.. pull-quote::

   There were some folk, Aristotle, Horace, and Cicero, who probably
   lived in ancient times.  They came up with a way of thinking about
   fiction and why we are able to appreciate a story even when we know
   it is not true to fact.

   Then another fellow Coleridge came along and coined the phrase
   "suspension of disbelief", which I think I've heard before
   and I like it; I'll remember it.

   Now the author of this book is suggesting that I apply a similar
   idea to *things I don't understand* rather than to *things that are
   not true*.  He comes up with an expression that is not nearly as
   good as Coleridge's phrase, so I won't remember it.  But I get his
   point: I need to be flexible as I learn, and not insist on fully
   understanding everything.

.. note::

   Exercise: learn as much as you can about Aristotle, Horace, Cicero,
   and Coleridge in 10 minutes.  Take very brief notes.

Building Connections
====================

While sitting in the uncertainty of a new topic or field is important,
you often aren't as lost or far from understanding as you think. Over
the course of your life, you've undoubtedly learned quite a bit about
being a human being, and it would be shocking if you found a source
that was entirely disconnected from that experience. The experiences
and knowledge you've gathered throughout your life will undoubtedly
come in handy as you try and approach other people's scholarship.

For example, when I first read Martin Heidegger's *Being and Time*, I
was daunted by the text. Heidegger essentially invents his own set of
vocabulary to tackle what he saw as problems with existing Western
Philosophy, which made it really hard to wrap my head around his
writing. However, many of his ideas on Being reminded me more and more
of my experiences of building relationships with other
people. Heidegger talks about truth as a sort of uncovering, of making
clear what was originally obscured. It made more sense to me when I
thought about the ways that people's character becomes clearer to me
when I interact with them alone rather than in a crowd of people. I
get to see their personality "uncovered," less inhibited by a need to
conform to the expectations of a large gathering. While my analogy
there isn't perfect, it helped me gain leverage in understanding
Heidegger's concept of uncovering in a way that just pouring over the
text alone wouldn't have.

When you connect what you're reading to your own experiences, don't think of
them as being exactly the same, but think of them as analogous. What between
your life and the object of study is similar? Even more importantly, where do
your experiences diverge from your object of study? Once you're able to start
identifying those points of divergence, you know that you're pushing past just
your prior experiences and into the topic of study itself!

It's also helpful to connect your topic to other things you've read or learned
about. In any field, you will find overlap and cross applications. You should
always be asking yourself if you can leverage anything from fields you already
have experience with to give you some sort of new knowledge in the subject
you're studying.

In knot theory, mathematicians found that physicists were using some of the same
mathematical structures as them to understand quantum fields, so the
mathematicians leveraged the techniques the physicists use on quantum fields to
understand more about knots.

These connections aren't just a way to develop understandings, they can also
serve as a basis for critique. When I make this connection between uncovering
and relationships, not only am I learning more about Heidegger, but I'm also
creating an opportunity for Heidegger to teach me more about relationships.
Physicists in the quantum field research are now using the mathematical
techniques developed by knot theorists to better inform their physics.

.. note::

   Exercise: learn as much as you can about Martin Heidegger.  Take
   very brief notes.


Where Is This Source Coming From?
=================================

An important thing to always turn a critical eye to is where your sources are
coming from. Everyone who is doing research has some set of biases underlying
their work. Some of these are benign or difficult to control for. Sometimes,
however, we can know that an author might have an incentive to make a certain
kind of conclusion. Any time you research a source, ask where the information is
coming from and why it is being presented the way that it is.

Sometimes, when you find a connection between a source and who made it, a
negative connection is clear. Famously, tobacco companies hire researchers
who very conveniently report that tobacco has much lower health risks than
researchers not hired by tobacco companies. The researchers have a financial
incentive to make their results favorable to the companies, because if they
produce research that says otherwise they risk losing funding.

Sometimes the source putting out an article doesn't seem to have much
bias, but the sources the article cites do.  `This article
<https://www.cnn.com/2020/11/08/tech/virgin-hyperloop-passengers/index.html>`_
from CNN reports on hyperloop, a train venture funded by Elon
Musk. CNN itself doesn't have any incentive to promote hyperloop, but
the only sources they cite are published by the company that produces
the train, and the only individuals who comment in the piece are
Virgin Hyperloop employees. This isn't a piece of journalism where the
journalist is critically asking serious questions of their
subject. Despite being on a "news" website, this article is more of a
press release from a company; an announcement made by Hyperloop on the
CNN platform.

Always pay attention to where an article is getting its information, and who
they are citing. Many times a political pundit or an interested party will be
framed as an expert source, but they can hardly be trusted to be impartial.
Always keep in mind that what might be driving the publication of an article or
other source could be more than just a good faith pursuit of truth.

In the appendix on research examples we have a link to the "Media Bias
Chart".  This is just one part of the picture of how to understand the
news source from which you are drawing information.


Building Blocks: Prefixes and Suffixes
======================================

Jargon is not without reason. Like all things, terminology is made up of
smaller parts. The English language is considered a Germanic language, meaning it
has been primarily influenced by German grammar and basic vocabulary. However, English
also borrows from many other languages, including Latin and Greek. 

Throughout Europe in the medieval and renaissance period, Latin was considered
p"the language of the learned" or "the language of scholarship." Tracing back to
the Roman Empire, Latin was the language of administration, law, literature, and
intellectual discourse. As Latin began to leave the vernacular, its roots stuck around
in scientific and academic language, so too with Greek. Thus, much of the jargon you'll
encounter will contain latin and greek roots, often in the form of prefixes and suffixes.

If you have a basic understanding of some commonly used greek and latin affixes,
you're ability to make educated guesses about the meanings of different terms
will multiple ten fold.

Here are some that are frequently used in scientific terminology:

Latin Prefixes
--------------

#. Anti-: meaning against (ie. antibiotic, antifungal)
#. Co-: meaning together (ie. coexist, cooperate)
#. Ex-: meaning out of, former (ie. exclude, ex-president)
#. In-: meaning not, opposite (ie. invisible, incapable)
#. Inter-: meaning between, among (ie. interact, international)
#. Per-: meaning through, completely (ie. permeate, promote)
#. Pro-: meaning before, in favor of (ie. proactive, promote)
#. Re-: meaning again, back (ie. repeat, regain)

Greek Prefixes
--------------

#. A-: meaning without, not (ie. amoral, atypical)
#. Bio-: meaning life (ie. biology, biodegradable)
#. Geo-: meaning earth (ie. geology, geography)
#. Hyper-: meaning over, excessive (ie. hyperactive, hypersensitive)
#. Hypo-: meaning under, below normal (ie. hypothermia, hypodermic)
#. Micro-: meaning small (ie. microscope, microorganism)
#. Neo-: meaning new (ie. neologism, neonatal)
#. Poly-: meaning many, multiple (ie. polygon, polygraph)

Latin Suffixes
--------------

#. -ology: meaning study or science of (ie. biology, psychology)
#. -tion/-sion: meaning act or state of (ie. reflection, decision)
#. -ism: meaning system, docterine, condition (ie. capitalism, communism)
#. -ity: meaning state or quality of (ie. integrity, diversity)
#. -ment: meaning result or product of (ie. development, improvement)
#. -able/-ible: meaning capable of (ie. readable, flexible)
#. -ate: meaning to make or cause (ie. activate, educate)

Greek Suffixes
--------------

#. -phobia: meaning fear of (ie. arachnophobia, claustrophobia)
#. -itis: meaning inflammation (ie. arthritis, bronchitis)
#. -graphy: meaning writing or recording (ie. photography, biography)
#. -meter: meaning measurement (ie. thermometer, speedometer)
#. -scope: meaning intrument for viewing (ie. microscope, telescope)
#. -cyte: meaning cell (ie. erythrocyte, leukocyte)
