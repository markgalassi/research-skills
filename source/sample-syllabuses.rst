===================
 Sample syllabuses
===================

In this chapter we present an aid for an instructor in Research
Skills: example syllabuses.

There are many effective ways in which one could teach the material as
a set of tutorials.  In the Research Skills Academy we have tried
several approaches, and we have imagined others.

Each section in this chapter shows an example of a syllabus based on
about 12 tutorials, of about an hour and a half each.

Galassi and Reeder 2024
=======================

Week 1
------

Tutorial 1:
   Housekeeping, the goals of the course, and what is critical
   thinking.

Tutorial 2:
   Nuance and the avoidance of platitudes.

Tutorial 3:
   Nuance and the avoidance of platitudes (continued).

Tutorial 4:
   Much ado about gender.

Week 2
------

Tutorial 5: 
   Writing.  Format is some work from the slides, but with pauses for
   students to do some exercises together.

Exercise:

Take this phrase from a typical article:

   In recent years, an increasing number of researchers have turned
   their attention to the problem of child language acquisition. In
   this article, recent theories of this process will be reviewed.



Higginson and Kerelis 2023
==========================

Karina Higginson and Albert Kerelis ran the Research Skills Academy in
2023 and used this syllabus as a blueprint.

Lesson 1: The History of Critical Thinking
------------------------------------------

Why teach this:
   Having an understanding of where ideas and practices come from
   gives a more holistic understanding of a topic, which is great, but
   also gives examples of how the skill that the students are actively
   learning has been used to change the world.
Main lesson:
   If you can master critical thinking and want to make a positive
   change in the way the world operates today, history is on your
   side. Our generation is hyper-aware of political and social strife
   due to over exposure and saturation of media, seeing historic
   examples of people using a skill from their tool belt inspires the
   ability to see and eventually actualize change. 

Pt. 1: Historic roots of critical thinking as a philosophical practice.
   Moad, Omar Edward. "Comparing phases of skepticism in al-Ghazali
   and Descartes: some First Meditations on Deliverance from Error."
   Philosophy East and West, vol. 59, no. 1, Jan. 2009,
   pp. 88+. Compares two early philosophers from different areas of
   the globe and their ideas that revolutionized how we think about
   thinking. While these philosophies have an undeniable religious
   element, this article does a good job focusing on the ideas of
   skepticism above all else. This is admittedly a difficult text, but
   I think our advanced high schoolers will enjoy the challenge as it
   is definitely still comprehensible.

   Potentially could focus on sections two and three, but the earlier
   does provide good context

   Why has it been important historically? Challenging the Church

   Andersen, Kurt. Fantasyland : How America Went Haywire : a 500-Year
   History. First edition. New York: Random House, 2017,
   pp.16-17. Offers a brief example of how Martin Luther inspired
   reformed protestantism, a direct result of critical thinking and
   skepticism of the churches current practices at the time... AND a
   religion that encouraged (some) critical thinking in it’s
   followers. NOTE: the language in this book can come across somewhat
   extreme, maybe find a different source

Pt.2  Modern Critical thinking and systemic change
   How critical thinking is/has been/can be used to destabilize
   institutionalized power

   Hooks, bell.  Teaching Critical Thinking : Practical Wisdom. New
   York: Routledge, 2010.

   "Decolonization" Pg. 23-28, highlights how questioning one binary
   or power structure often leads to questioning other ways that same
   structure oppresses other groups. Critical thinking about one issue
   can lead to the liberation of others.

   “Feminist Revolution” Pg. 91-94 highlights the role of lower and
   high education, how biases affect it, how it can change, and how it
   has changed

Lesson 2: Keeping Research Tidy
-------------------------------

In this lesson students will learn how to find and manage information on their
research topic. Research is only as good as the sources we access, and we can
only make use of those sources when we can manage them effectively. By learning
how to query academic databases, students will gain access to a wider array of
quality sources of information and be able to use them effectively.

Students will also learn to use Zotero, a free and open source library and
citation management tool, to track and organize their sources and generate
bibliographies. By using a citation manager, students won't waste time trying
to find sources they forgot to write down, and will be able to better parse
a long list of sources by using tags and notes.

Note taking is another critical skill in conducting library research, and by
learning to experiment with their note taking strategies students will be able
to find a note taking strategy that works for them. Good notes not only help
us understand our material as we read, but also help us reference our findings
quickly and accurately. They are a key tool in making as much of the information
we've gathered from our research available to us as seamelessly and
conveniently as possible.

Lesson 3: Good Writing: What it is, How to recognize it, and How to do it
-------------------------------------------------------------------------

Why teach this:
   Being able to effectively and clearly communicate your thoughts is
   a tool needed for any field, but also for life. 

Pt.1: What makes good writing good
   Samberg, Joel. “Bad Writing Inc.” BusinessWest 32, no. 27 (2016): 14–.

Pt. 2: How to recognize good and bad writing
   What it means, when referring to writing integrity, to do something
   in good or bad faith

   Journalism in the age of the internet
   - Clicks over integrity 
   - Rage baiting 

Pt. 3: How to do good writing
   Good writing in narrative

   Abell, Stig. “Get to the Point: Irina Dumitrescu and Sam Leith on
   How to Write Well.” TLS. Times Literary Supplement (1969), no. 6101
   (2020): 1–.

   Finding your voice, make bad habits charming (while still
   grammatically correct)

   Exercise: Writing your writers manifesto 

   Williams, Paul. “A Writer’s Manifesto: Articulating Ways of
   Learning to Write Well.” New Writing (Clevedon, England) 17, no. 1
   (2020): 71–79. https://doi.org/10.1080/14790726.2019.1566366.


Lesson 4: Progress
------------------

Relevance:
   Much of our economic system today is propelled by an idea of
   progress. That the more we create and invent, the better our lives
   will necessarily be. However, this idea that more technological
   innovation is necessarily a good idea has led to the creation of a
   wildly unjust and violent economic system. Without taking time to
   step back and understand the impacts of the systems we participate
   in, we risk complacency with mass violence.


* Max Weber saw industrialization and the movement of people into
  factories not just as something which increased production, but also
  turned peasants into laborers.

  - While industrialization made it possible for one laborer to make
    far more of a product that was possible before, they didn't spend
    less but more time working!
  - Manufactured goods were cheaper to aquire, but the quality of life
    for those making them decreased.

* In One Dimensional man, Herbert Marcuse takes note of how technical
  progress had diverged from its original stated aim - the
  amelioration of human life.

  - For Marcuse, technology is no longer primarily something which we
    produce to make our lives better, but is something which creates
    necessities for us.

* Climate change is a great example of the massive unintended
  consequences of our technological advances
* The silicon valley motto "move fast and break things" is great when
  what you're breaking is some new video game or entertainment system,
  but when what you're breaking is the social fabric of myanmar
  because your social media platform doesn't have a system of
  moderation and is allowing for hate speech to spur a genocide, you
  have to question wether the ends of technological progress
  unilateraly justify its means.
* But obviously some technology is good - how do we progress responsibly?

  - Ecological sustainability - by forefronting questions of how
    sustainably a technology can grow, we can avoid destroying our
    planet.
  - Afrofuturism - science-fiction has always spurred forward
    technological progress from the submarine to the
    metaverse. Afrofuturism has provided a way for Black writers to
    envision what technological progress that is inclusive of
    marginalized people could look like.

Lesson 5: Measurement, Method, and Parsimony
--------------------------------------------

Relevance:
   How we evaluate truth claims is central to our idea of research. To
   some extent, science simplifies Truth for us: p<0.05. While the
   rigors of the scientific method allow us to make truth claims about
   our world really effectively, they can sometimes obfuscate mistakes
   from us if we're not careful about it. By looking into how science
   measures and interprets data, we can better understand what studies
   are really looking at.

- prediction and parsimony

  - "A key feature of scientific ideas, as opposed to other types of
    ideas, is not whether they are right or wrong but whether they are
    logically coherent and make unambiguous, observable, and generally
    quantitative predictions. They tell us what to look for and
    predict what we will find if we look at or measure it." CLUE -
    Chap 1

    - Scientists look for the simplest possible explanation that
      predicts accurately - parsimony

      - Analogous to okham's razor

	#. In evolutionary biology, we can build trees of relation
           between species, locating which ones had more recent common
           ancestors, by cataloging traits of each of the species and
           building that tree of descent which requires the fewest
           mutations.
	#. Have people download mesquite and do a parsimony simulation?

- `Pseudoreplication <https://faculty.washington.edu/skalski/classes/QERM597/papers/Hurlbert.pdf>`

  - This landmark ecology paper critiqued how ecologists had historically worked on experimental design.
  - It raises really great questions about how we structure trials
    that allow for deep investigation into what a modern data driven
    scientific approach to understanding looks like and where it might
    go wrong.
  - Leads us to a question about what we are actually measuring when we collect data
- Operationalism!

  - Here's a piece I wrote for the Quest a long time ago that goes
    into some fun detail on how scientific measurements work.

    - https://reedquest.org/2021/04/02/albertoperationalism/

      - Basically, science can only ever observe interactions between
        its subject and its measuring device.
      - Scientific data exists primarily in the in-between, not in the
        thing itself

Lesson 6: Cultivating Curiosity
-------------------------------

Why teach this:
   As future researchers and academics, curiosity is a fundamental
   attribute the students should be cultivating.
The five dimensions of curiosity and their motivators
    Jones, Dan. “How to Be Curious.” New Scientist (1971) 256,
    no. 3408 (2022):
    38–43. https://doi.org/10.1016/S0262-4079(22)01862-0.  
The benefits of curiosity
   - Creativity and curiosity often go hand in hand
   - Reduced anxiety with a curiosity based reframing 
   - How to avoid judgment (the killer of curiosity)
   - What might be limiting our curiosity? Often, it can be biases,
     conscious and unconscious, that create fully formed and unmovable
     opinions on something without due diligence

What are biases
   - How are they created
   - How can we recognize our own and others?
   - How can we challenge our own and others?
   - Why curiosity makes some people uncomfortable
Requires them to think critically and sometimes independently
  This often comes with a lack of validation from others
Beliefs have kept them safe
   Whether perceived or actualized danger, some belief systems are
   held so strongly by people because it currently or at some point
   has provided safety or security for them 
Disagreement
   It’s possible to disagree but respect why people may be hanging on to
   their beliefs, after all, it’s about your journey not theirs.


Lesson 7: Epistemology, Hermeneutics, and Pragmatism
----------------------------------------------------

 Understanding how and why we evaluate truth claims is central to well, existing
 in the world. However qualitative humanistic data requires different methods
 than science has to offer. Hermeneutics and Pragmatism offer two sorts of
 reckoning that expand past quantitative measurement and into the examination
 of human art and culture.

In this lesson, students will learn about three concepts that can help us parse
information and make sense of the world around us.

Henri Bergson's intuitive approach asks that we first approach problems from
the level of the question they are trying to answer. Often we take questions
to be a neutral starting point for inquiry, but Bergson tells us that questions
can carry with them assumptions and contradictions that can easily find their
way into our work if we're not careful.

The hermeneutic circle is a concept that takes many forms for many thinkers,
but its basic premise is that better understandings are always generated from
partial misunderstandings. Our knowledge of a subject never comes to us ex
nihilo, and we must both be confident enough to procede with incomplete
knowledge, as well as humble enough to recognize it as such. The hermeneutic
circle tells us that understanding is not a final destination of perfect
knowledge, but is constituted by a continued striving for better understanding.

Pragmatism gives us a simple yet powerful evaluative framework for our ideas. It
sees knowledge, ideas, and truth, as tools. Pragmatists evaluate claims based
on how well those claims let us achieve our goals and attain more useful
worldviews. This leaves us with an immense responsibility in how we define our
goals. We need to have an idea of who we want to be, what kind of things we want
to achieve, before we can be good pragmatists. But once we are able to attune
ourselves to a sense of that, pragmatism lends a powerful tool in parsing
information and making sure our knowledge and truth align with our values.

Lesson 8: Introduction to Absurdism
-----------------------------------

Why teach this:
   Entering early adulthood, you’re told to start searching for your
   purpose, “what are you here to do?” Absurdism offers an alternative
   to narrowly defining your existence to a purpose, and to only one
   of them.

   All based off works by Albert Camus, especially The Myth of Sisyphus
Pt. 1: God is dead 
   the scientific revolution brought about nihilism, the philosophical
   idea that according to science, there seems to be no God. Since
   before then, all moral and ethical questions, as well as lifestyle,
   had been answered according to religion. For people of science
   though, this no longer seemed like the correct solution. Man
   discovered life has no prescribed meaning.

   As a reaction to nihilism, two schools of thought formed,
   existentialism and absurdism, existentialism (Existentialism is
   Humanism by Jean-Paul Satre) believe our actions create life's
   meaning and our purpose.

Pt. 2: Absurdism
   (I find when teaching philosophy, it works well to lean into quotes
   so the students have something visual to parse and understand
   rather than just talking abstractly. Parsing quotes give philosophy
   a place to ground itself. Ideally these quotes would be from Satre
   and Camus.)

   Absurdism does not try to find meaning or purpose, but rather
   enjoys the freedom of a meaningless existence

   Trying to find meaning is philosophical suicide

   The only “purpose” of life is to live it, to do whatever it is that
   prevents you from killing yourself

   AAA I LOVE ABSURDISM!!

Pt. 3: Puzzles
   (this would work best as a round table discussion, as there are no
   defined answers to these huge questions!)

   How do we grapple ethics? 

   How do we operate in a very un-absurdist social structure? 

   What are the places absurdism falls short?


Lesson 9: Energy and Thermodynamics!!
-------------------------------------

Relevance:
   Some basic understandings of Energy are super important for
   building models of how scientific systems work. The principle of
   minimum energy is one of these big central ideas that gets used
   across scientific disciplines. While it's simple to state, the
   minutae of the second law of thermodynamics can give us a more
   rigorous understanding of what energy is and how we apply these
   concepts.

- Understand energy graphs
  - Show in chemistry - fusion, bonding
  - But also in physics - potential energy in gravitational systems
- Activation energy!
  - Definitely a chemistry concept but applicable to so many fields
- Entropy
  - Second law of thermodynamics
  - S = k log(W)
  - Micro-states and macro-states
- Relate to other systems

  - Ecology!

    - Biodiversity = stability

      - On a big picture scale, we can understand this from an entropy lens
      - Those ecosystem macrostates which have the most possible configurations or microstates will be more likely to arise.

    - Alternative stable states

      - When ecosystems degrade, they are often put into states that are hard to get out of, even if they are not the most stable, because they would have to transition through unstable states!
    - Disturbance regimes
      - Many ecosystems tolerate and in fact depend on occasional disturbances as long as those don't pull them entirely out of their stable state
  - Economics
    - Corporations and other economic entities often work to minimize costs, which are kind of like energy in an economic sphere.
  - These concepts can also be used in coarse non-quantitative ways.
  - Minimum energy can also be thought of as following the path of least resistance
    - People tend to follow the path of least resistance! We do what's easiest for us.
    - This is a super useful rule of thumb for everything from navigating your relationship with others to good game design.

Lesson 10: Poetry and the space between life and death
------------------------------------------------------


Why teach this:
   What better way to study the power of words than to see it’s
   potential to move the reader into a metaphorical realm that exists
   outside of time and space!***I’m using an essay I wrote about this
   topic, so there’s much more detail and sources to add here, just
   wanted to get the general outline.

Pt. 1: “Here,” “There,” and everywhere
   Location descriptors act metaphorically to create a location, the
   location of the poem, that exists outside of time and space. It
   refers to the here of the reader, the here of the word on the page,
   and the here of the speaker

Pt. 2: The Lyrical Present
   Locates the reader in the poetic realm, where they are
   participating in the action with the speaker. “I walk” as opposed
   to “I walked.”

Pt. 3: Immortality through words
   The poets version of themselves, the speaker, can exist as long as
   there are people reading their poems in that poetic realm. You can
   put that version of yourself, the reader, into that poetic realm
   that exists outside of time and space, and walk with the speaker
   and by extension the poet.

Lesson 11: The Surrealist Art Movement and Imagination
------------------------------------------------------

Why teach this:
   The Surrealist Art movement prioritized creativity and imagination,
   which as we know from earlier, goes hand in hand with
   curiosity. Art for the sake of art, and learning for the sake of
   learning. **I’m using an essay I wrote about this topic, so
   there’s much more detail and sources to add here, just wanted to
   get the general outline.**

Pt. 1: The history and philosophy
   Started in the 1930s france

   About allowing the unconscious mind to explore, emphasis of creativity and imagination
   Leaning into the impossible

Pt. 2: key works
   (show key pieces, and artists and a brief progression of their works over time)

   How did this artist embody or shape the surrealist art movement

Pt. 3: the value of imagination in an industrial world
   Surrealists valued creativity for purely its own sake

   A rebellion against commodification, artists who had the skills to
   capture accurate portraits or realistic landscapes (and could have
   gotten paid to do so) instead chose to make art that was just for
   enjoyment

Lesson 12: Gender and Queer Theory Primer
-----------------------------------------

Relevance:
   Taking a critical eye to gender is important both personally and
   socially. By understanding how gender both gratifies and constrains
   us, we can develop sounder and healthier relationships with
   ourselves and expectations of us. By understanding how gender has
   been historically constructed, we are better situated to intervene
   in instances where ideas of gender are not serving us or our
   communities.

   I have a whole presentation I gave to a philosophy club at my
   highschool a couple years ago ready to go, replete with resources
   for further study.
   https://drive.google.com/drive/folders/1la5dCgxDn7xPGfBVXwa80srXa8APQRTa?usp=sharing

Here are some rough outlines for lessons. I tried to include the main
talking points and some extra sources, as well as different methods of
teaching that might work best.
