=============
 The Academy
=============

The idea for the Research Skills Academy came from a series of
conversations with co-founder and director Mark Galassi and a strong
group of young students (8th grade to 12th grade).

In our conversations, we discovered there were missing links in
preparing students to do research.  There was a need for a course that
would inject students into higher education and other academic
institutions by teaching skills that high school does not contemplate.

We then formulated what a useful collection of skills and background
knowledge would be for motivated high school students and
ultra-motivated middle school students interested in academic
research.  This group of skills became the course material for the
Research Skills Academy.

We have some goals in mind for what students should pick up in this
program: research skills, a habit of critical thinking, exposure to
several different career paths, and how research enters into those
paths.

The students had a lot of input on the schedule, intentionally
designing it to stimulate their age group and not be too exhausting
for a high intensity summer program.

The Schedule
============

Students in the Research Skills Academy commit to about five and a
half hours of work per day, four days a week, for three weeks. The
specific durations mentioned below are just an approximation.

Each day starts with a tutorial lecture by a director of the program,
Mark Galassi, Karina Higginson, or Albert Kerelis, or new directors
who join us.  This starts at 10am in our main time zone (US/Mountain
time) and lasts about an hour.  These lectures cover a wide range of
topics, including workshop skills, soft skills, and theoretical
discussions.  The tutorials are often based on chapters in this book,
but some are separate.  To see a brief overview of the lessons, visit
the :ref:`sample-syllabuses:Sample syllabuses` appendix.

Students then have a period of individual work on a long-term research
question, or other areas of interest.  In this period they are
encouraged to invite collaboration from other students on the project
they are forming in their mind.  This is largely done using chat rooms
and document sharing, and is a little over an hour long.

After a 45 minute lunch break, there is a 15 minute "scrum" meeting,
guided by the Acadmey directors Karina Higginson and Albert Kerelis.
In these meetings, students discuss and plan tasks with their peers
and the senior members present so that their work is guided, efficient,
and effective.

After the scrum it's back to individual, or self-formed small group,
work on a topic. This is intended to be focused on producing written
results, and goes for about a hour.

The day ends with a career path lecture. Running about an hour and
with 15 minutes for questions, scholars and professionals in very
diverse areas speak about what careers in their respective field can
look like, as well as current questions or issues in their field. The
hope is to inspire students to think about potienetial careers, while
also engaging their curiosity in potienital research questions. After
this lecture, one more hour or so of independent work. The day will
typically end around 3:45 PM.

What is Research?
=================

For our purposes, research is *not only* scientific research that produces 
original results, such as experiments or data collections. Research is much 
more often the *process* of carefully studying a problem or an area of knowledge 
with the goal of understanding it clearly.

These two uses of the word "research" are linked, one acknowledges the new 
knowledge obtained while the other acknowledges the process of obtaining it.
Original research is often a lengthy endeavor, and in the course of it a 
scholar will need to research many and varying academic fields that contribute 
to solving their question or problem.

A classic example of cross-field research is Albert Einstein's path to
formulating the theory of General Relativity. A key component in
developing General Relativity was a field of mathematics called
differential geometry. Einstein did not know much about it, so he
asked his old friend the mathematician Marcel Grossmann to research
whether an appropriate, non-Euclidean, geometry existed. Einstein's
biographer Abraham Pais [Pais1982]_ reports the story:

.. pull-quote::

   [...] he told Grossmann of his problems and asked him to please go
   to the library and see if there existed an appropriate geometry to
   handle such questions. The next day Grossmann returned (Einstein
   told me) and said that there indeed was such a geometry, Riemannian
   geometry. It is quite plausible that Grossmann needed to consult
   the literature since, as we have seen, his own field of research
   was removed from differential geometry.

Grossman and Einstein researched the existing state of mathematics
surrounding the geometry of curved spaces. Grossman did traditional
*library research*, which taught him of the existence of a new branch
of mathematics. While not Einstein's original research question,
researching new areas of geometry was crucial in enabling Einstein's
original theory on Relativity.

As demonstrated, no research question is limited by it's field. In any
academic project you will need to research a variety of topics, and it
is important that you get comfortable doing so with efficiency and
precision.


Transfer of metaphor
====================

A key idea in the Research Skills Academy is something I call
"Transfer of Metaphor".  We want to look at how *ideas born in one
area can be applied in other areas.*

Examples of this include:

natural selection
   An idea from *biology* which plays significant roles in social
   anthropology (evolution of human social behavior), psychology
   (evolutionary psychology), computer science (genetic algorithms and
   artificial life), and other fields.
narrative
   An idea from *literature* which is pervasive in other fields,
   including computer programming.  One of the great virtues of
   narrative in other fields is in helping to *tame complexity:* if a
   story can be told clearly, then new people joining a project can
   assimilate it more easily.
entropy
   An idea from *physics* which comes up in biology and computer
   science.
cell
   An idea from *biology* which comes in computer science, having
   inspired the development of *object-oriented programming.*

In general we should be hyper-alert in our minds on whether something
we have learned in one field can be used in others.  Sometimes one
refers to this as "making connections".


What Students Should Produce in this Time
=========================================

The general goal of each day is to produce some written work that
demonstrates what a student has learned during their independent
research time. What exactly that written product looks like can vary
depending on the student's inclinations and what makes sense for their
project.

A simple approach could be to pick a topic in the morning, spend most
of the afternoon doing research, and then taking the last 30 minutes
of the day to write a position paper or research summary on it.

Alternatively, a student could put together a short lecture on their
topic, add to a written product that spans multiple days of research,
or write an experimental research proposal based on what they've
learned.

The idea is not to have a rigid rubrick for what a student must
complete, but to have each student get in the habit of synthesizing
and communicating what they've learned in a way that makes sense
for them and their project.

As a student you might find this independence daunting at first.  We
hope that you will find it to be what makes this experience
remarkable.


The Slant of the Lectures
=========================

Below is the simple blurb we send to our lecturers when we ask them to
present to the Research Skills Academy:

.. pull-quote::

   The purpose of lectures at the Research Skills Academy is to give
   students an overview of what careers look like in the speaker's
   area of scholarship.

   We also encourage speakers to include discussion of two aspects of
   their area: (a) "out of the box" thinking about it, which would
   demonstrate how it is different from what people imagine; (b)
   social justice ramifications and how a student can plan a career
   that improves their sector of the world.

   But in the end it is entirely up to the lecturer -- slides,
   fireside chat, ..., any format they pick is good.

The lecturers are professionals in their field eager to share their
insider knowledge with you. Ranging across many fields, these lectures
are meant to inspire and inform not only in content, but also in style
of presentation.  You will have to give your own lecture at the end of
the Academy, so take mental note of what styles of lectures work well
for you, and which you might want to emulate.

Computing, Platforms, and Software Freedom
==========================================

The Institute for Computing in Research is deeply rooted in software
freedom, and its interns work entirely with free/open-source software
(FOSS).  The Research Skills Academy uses an entirely FOSS
infrastructure, but does not require the use of a complete FOSS
platform by students (although we do offer those tools).

While students aren't required to use FOSS, some of the discussion in
the Academy revolves around digital citizenship, vendor lock-in, and
other areas in which software freedom plays a key role.

For our purposes computers will be used as a tool for communicating,
researching, writing, visualizing information, and (quite rarely)
examining source code and running it.

The two programs we will use beyond a web browser and office programs
will be (a) a plotting program (for when we visualize data), (b) the
Python language interpreter (for when we see what a computer program
looks like), and (c) version control.

For plotting there are many tools available.  We have prepared some
information on is RAWGraphs.  RAWGraphs works in any modern web
browser, so it will be available to users of all platforms.  You will
find a tour of RAWGraphs in :ref:`app-logistical-details`.
