.. Research Skills and Critical Thinking documentation master file, created by
   sphinx-quickstart on Fri May 14 21:28:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************************************
 Research Skills and Critical Thinking
***************************************

.. only:: html

   -----------------------------------------
   (A Guide for the Research Skills Academy)
   -----------------------------------------

   :Date: |today|
   :Author: **Mark Galassi** <mark@galassi.org>
   :Author: **Karina Higginson** <karinajhigginson@gmail.com>
   :Author: **Albert Kerelis** <ajkerelis@reed.edu>
   :License: This is a free book.  You may adapt and redistribute it
             under the terms of the Creative Commons
             Attribution-ShareAlike 4.0 International license (CC
             BY-SA 4.0) described at
             https://creativecommons.org/licenses/by-sa/4.0/


.. raw:: latex

   \doclicenseThis
   \begingroup
   \sphinxsetup{%
         verbatimwithframe=false,
         VerbatimColor={named}{OldLace},
         TitleColor={named}{DarkGoldenrod},
         hintBorderColor={named}{LightCoral},
         attentionborder=3pt,
         attentionBorderColor={named}{Crimson},
         attentionBgColor={named}{FloralWhite},
         noteborder=2pt,
         noteBorderColor={named}{Olive},
         cautionborder=3pt,
         cautionBorderColor={named}{Cyan},
         cautionBgColor={named}{LightCyan}}

.. caution::

   This web book is a work in progress.



.. toctree::
   :maxdepth: 3
   :caption: Contents:

   motivation.rst
   critical-thinking.rst
   reading-effectively.rst
   data-visualization.rst
   research-step-by-step.rst
   cognitive-factors.rst
   workshop-skills.rst
   products-written-and-verbal.rst
   academic-awareness.rst
   ethics.rst
   the-research-cemetery.rst
   thought-provoking-books-and-media.rst
   research-examples.rst
   sample-syllabuses.rst
   app-logistics-to-get-going.rst
   app-list-of-articles-for-discussion.rst

   bibliography.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
