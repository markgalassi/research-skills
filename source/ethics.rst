=================
 Relevant Ethics
=================

The Big Picture of What We Do
=============================

The Research Skills Academy was created to  fill gaps in knowledge about how
to properly conduct research. The Academy's goal is  a bit more nuanced than
that though, naturally.

On `our website
<https://computinginresearch.org/research-skills-academy/>`_, you can
find our goals. A quote from that section reads: "almost all the
prestigious research opportunities for youth go to students from
privileged families who have connections to laboratories and
universities."

The road to diversity is long and winding. We live in a world in which diversity or 
inclusion has only recently become a part of the conversation, and, in a lot of ways,
we're playing catch up.

Our goal at the Research Skills Academy has many components, but is ultimately
dedicated to making valuable opportunities available to as many people as possible.
This explains our relatively low barrier to entry, our promotion of the public 
library, and the remote nature of the program.

Respect and Codes of Conduct
============================

At the Institute for Computing in Research, we adapted our code of
conduct from the `Contributor Covenant
<https://www.contributor-covenant.org/>`_. You can read our full code
of conduct `here <https://computinginresearch.org/code-of-conduct/>`_.

Workplace Behavior
------------------
When working in a team on a project or for a company, your personal ethics are
not the end all be all. For example, if you agree to be part of a project, you are
agreeing to *their* rules (often, via a signed document or contract). Therefore,
you must follow them, even if you don't agree with all of them. Keep in mind, this
is not to say you should abandon your own ethics for a job or project, rather, be
aware of what kind of environment you are agreeing to be a part of. Once you've
made that agreement, honor it.

If you find yourself in a situation where you believe your peers, superiors, or
the company at large are participating in unethical practices, there will
often be systems than can give you an opportunity to speak up (for example, Human
Resources). However, you can also report externally, to regulatory bodies, unions
or law enforcement, depending on the severity of the issue and the companies
response to concerns. Whether you raise concerns internally or externally, it
is important to remember that while you have to follow rules you agreed to, you
always have the ability to speak up if something seems unethical to you. *Your
voice and opinion matters!*

.. sidebar:: Extra Credit

    Think of a time you really enjoyed working with someone, perhaps a group
    project or partner work in class. What did you enjoy about their
    work attitude? What about it meshed well with yours?

    Alternatively, think about a time in which you *didn't* enjoy working with someone.
    What about their attitude did you not enjoy? How did it effect the quality of the work
    produced?

    Who are some mentors or teachers in your life that you look up to? What approach 
    do they take? How might you be able to replicate it? 

Be kind. Treat others how *they* want to be treated.
No one wants to work with someone unpleasant, rude, or disrespectful.
Often, media presents us with a picture of the successful entrepreneur: a blunt,
cocky, and slightly cold white man in an expensive suit, drinking whiskey in
an office overlooking a cityscape. The people in these positions of power are
praised for being "hard," a "boss," or even "ruthless". However, this model
is only one example of a type of attitude in the workplace. While occasionally
effective for wealthy white guys, this style often does not produce the same
results when executed by a person in a minority group. For example, a woman
who might behave similarly to her head-strong male co-worker might be labelled
"difficult to work with", "moody", or "mean". While there might be no one
right way to be successful, it's important to make sure you are showing up in a
way, in and outside of your workplace, that is true to your values and
compassionate. Sometimes the most effective leaders
do so through kindness, flexibility, and empathy.


Plagiarism: Don't Do It.
========================
One thing you will hear consistently from middle school all the way
through graduate programs is to never plagiarize. You might already have
an understanding of what it is, and how to avoid it, but it is  worth repeating.

What is Plagiarism?
-------------------

Plagiarism is the act of using someone else's ideas, words, or
work without giving them proper credit. Of course, ideas
naturally build on each other. However, something becomes plagiarism
rather than the natural evolution of ideas when the original idea is
not properly cited. The point of citations is not cause you headaches,
but to make sure that people who work to create and write down ideas are
recognized for their work.

Here are a few examples of plagiarism:

#. Copying and pasting from a source without citing it.
#. Paraphrasing someone else's ideas or concepts without giving
   credit.
#. Using images, graphics, or illustrations without proper
   attribution.

Before you go to publish or share any piece of work you do, ask yourself if all
of the ideas you wrote down were really you're own. If you feel like you're
indebted to any particular sources, cite them! The authors of those sources helped
you learn and grow. The least you can do is give them credit where it's due.

To avoid plagiarizing, it is of utmost importance that you learn proper citations.
There are many free online resources that will do the citations for you, or at least
get you started. These generators can be incredibly helpful and a good place to start,
but often they miss a few things or mistake different versions of the same texts.
So, it's also important you learn to do it for yourself, if for no other reason than 
to be free from the machine.

You can find more discussion of citations in :ref:`workshop-skills:Citations`

Putting Yourself in Their Shoes
-------------------------------
Picture this: You've just graduated with a masters in your field. You are excited,
passionate, and eager to begin your own research project (not unlike the position you
find yourself in at the Research Skills Academy). You finally have the
freedom to explore the nooks and crannies of your beloved subject. You research and write,
and research and write, and research some more, until you have finally produced
your masterpiece. Full of original ideas and new findings, your article sludges
through the grueling peer-review process and by some miracle gets published and 
verified. 

All this disappears when you plagiarize. Not only are you being dishonest with 
yourself and others, but you are actively discounting someone else's, someone who 
you probably actually have a deep respect for, countless hours of work. If you wouldn't
like it to be done to you, don't do it to someone else.

Scary Campfire Stories
----------------------
The German politican and defense minister Karl-Theodor zu Guttenberg was found guilty
of plagiarizing large portions of his doctoral thesis. After it had gotten wide spread
acclaim, an internet user noticed extensive plagiarism. He had copied sentences, paragraphs,
and even whole pages with no citations. Gaining national and international media attention
and being extensively reviewed by academic institutions and organizations, he was found
guilty in March 2011. This caused his University to *take back his doctoral degree* and
a *forced resignation* from his position as German Defense Minister. 

His reputation, career, and what he had been working towards for six years were all taken
away from him. Scary? It should be! Do. Not. Plagiarize.

The Beauty of Citations: Sharing is Caring
------------------------------------------

It's a little kitsch, but true. Properly citing your sources makes you part of
a larger academic conversation. The author of the text you're citing gets to
delight in seeing how others expand on their findings, the readers of your
research have resources to fuel their curiosity, and your claims are
legitimized as well-informed.

Research, and academia in general, is at it's core a collaborative endeavor, and it's
beauty is in that collaboration!

Established Ethical Guidelines in Certain Fields
================================================

Different fields have different ethical guidelines, naturally. They all
general revolve around protecting the subject, patient, and consumer. In order
to learn the ethical guidelines of your respective field, there's a few places
you can look.

The best place to start is Professional Association websites. Most fields have
an organization or assocaition, such as the American Psychological Association
(APA), or the American Medical Association (AMA). These organizations are made
up of professionals in their respective fields who come together to decide on
standards for the practices. Some other assocaition websites you might find
helpful are the Association for Computing Machinery (ACM) and the National
Society of Professional Engineers (NSPE).

If you have no luck there, you might try government agencies and regulatory
bodies. For example, the World Health Organization (WHO) provides guidelines
for medical research and drug development.
