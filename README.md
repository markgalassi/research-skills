First you need the general tools to make sphinx documents:

pip3 install --user sphinx sphinxcontrib-bibtex sphinx-rtd-theme sphinxcontrib-programoutput sphinx-multitoc-numbering

I use the cartouche extension

pip3 install --user cartouche sphinxcontrib-twitter

After installing that you can build the various formats of the book
with:

```
   make html
```

and you can look at the file build/html/index.html with your browser.
